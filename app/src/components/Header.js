import React, { Component } from 'react';
import { Appbar } from 'react-native-paper';

export default class Header extends Component {

    render() {
        return (
            <Appbar.Header style={{ backgroundColor: 'transparent', elevation: 0 }}>
                { this.props.goBack && <Appbar.BackAction color='#000' onPress={this.props.goBack}/>}
                <Appbar.Content color='#000' titleStyle={{ marginRight: 'auto' }} title={this.props.title}xd />
            </Appbar.Header>
        )
    }
}