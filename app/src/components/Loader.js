import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-paper';

export default class Loader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false 
        }
    }

    show() {
        this.setState({ show: true })
    }

    close() {
        this.setState({ show: false })
    }

    render() {
        return (
            <ActivityIndicator animating={this.state.show} size={'large'} />
        )
    }
}