import React, { Component } from 'react';
import {View, Platform} from 'react-native';
import {Button} from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

export default class DatePicker extends Component {

    constructor(props) {
        super(props);
        this.state = { show: false };
    };

    show() {
        this.setState({ show: true })
    }

    render() {
        return(
            <View>
                <Button 
                    onPress={() => this.show() } 
                    mode="outlined" color="#000"
                >
                    {this.props.mode == 'date' 
                        ? moment(this.props.value).format('DD/MM/YYYY').toString()
                        : moment(this.props.value).format('HH:mm').toString()
                    }
                </Button>

                { this.state.show &&
                    <DateTimePicker 
                        value={this.props.value}
                        mode={this.props.mode}
                        is24Hour={true}
                        display="default"
                        onChange={async (event, selectedEvent) => {
                            await this.setState({ show: Platform.OS === 'ios' })
                            this.props.onChange(event, selectedEvent);
                        }}
                        minimumDate={new Date()}
                    />
                }
            </View>
        )
    }
}