import React, { Component } from 'react';
import { View } from 'react-native';
import { TextInput, IconButton } from 'react-native-paper';

export default class Input extends Component {

    render() {

        return (

            <View style={{ flexDirection: 'row' }} >

                {this.props.icon && 
                    <IconButton 
                        icon={this.props.icon}
                        color={"rgba(0,0,0,0.54)"}
                        disabled={false}
                    />
                }

                <View style={{ flex: 1 }}>
                    <TextInput
                        style={{ elevation: 2, backgroundColor: '#edeef0' }}
                        dense={this.props.dense}
                        mode={this.props.mode}
                        placeholder={this.props.placeholder || ''}
                        placeholderTextColor='#444'
                        secureTextEntry={this.props.secureTextEntry || false}
                        textContentType={this.props.textContentType || 'none'}
                        keyboardType={this.props.keyboardType || 'default'}
                        value={this.props.value}
                        onChangeText={this.props.onChangeText}
                    />
                </View>
            </View>

        );
    }
}
