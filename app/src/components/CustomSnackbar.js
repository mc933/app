import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Snackbar } from 'react-native-paper';

export default class CustomSnackbar extends Component {

    constructor(props) {
        super(props);
        this.state ={
            show: false
        }
        this.onClose = null;
    }

    componentDidMount() {
        if(this.props.onRef != null)
            this.props.onRef(this)

        if(this.props.onClose != null)
            this.onClose = this.props.onClose
    }

    show() {
        this.setState({ show: true })
    }

    close() {
        this.setState({ show: false })
        if(this.onClose)
            this.onClose();
    }

    render() {
        return (
            <Snackbar 
                wrapperStyle={styles.bottomView}
                style={this.props.error ? styles.error : styles.success}
                duration={2000}
                visible={this.state.show}
                onDismiss={() => this.close()}>
                    {this.props.text}
            </Snackbar>
        )
    }
}

const styles = StyleSheet.create({
    success: {
        backgroundColor: '#4bb543'
    },

    error: {
        backgroundColor: 'rgb(255,0,0)'
    },
    
    bottomView: {
        marginTop: 10
    }
})