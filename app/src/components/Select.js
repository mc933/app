import React, { Component } from 'react';
import { View, Platform, StyleSheet } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {Picker} from '@react-native-community/picker';
import {IconButton} from 'react-native-paper';

export default class Select extends Component {

	render() {
		
        let optionsArray = [ ...this.props.options];
		optionsArray.map((data, index) => data.key = String(data.value));
		let options = this.props.options.map(function(obj, index){
			return(<Picker.Item label={obj.label} value={obj.value} key={index} />);
		}); 

		return (
			<View style={styles.content}>
                
                {this.props.icon &&
                    <IconButton 
                        icon={this.props.icon}
                        color={"rgba(0,0,0,0.54)"}
                        disabled={false}
                    />
                }
				{Platform.OS == 'ios'
					? <View style={styles.pickerContent}> 
                        <RNPickerSelect 
                            style={{ 
                                inputIOS: {
                                    backgroundColor: '#edeef0',
                                    paddingVertical: 14,
                                    paddingLeft: 10,
                                    fontSize: 16,
                                    color: this.props.value == '' ? 'rgba(0,0,0,0.54)' : '#000'
                                }  
                            }}
                            placeholder={{}}
							onValueChange={this.props.onChange}
							value={this.props.value}
							items={optionsArray}
						/> 
					</View>

					: <View style={styles.pickerContent}>
						<Picker
                            style={{ 
                                backgroundColor: '#edeef0',
                                color: this.props.value == '' ? 'rgba(0,0,0,0.54)' : '#000'
                            }}
							selectedValue={this.props.value}
                            onValueChange={this.props.onChange}
                        >		
						    {options}
						</Picker>
					</View>
				}

			</View>

		);
	}
}

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        marginTop: 5,
    },
    pickerContent: {
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.54)',
        borderRadius: 3
    }
})
