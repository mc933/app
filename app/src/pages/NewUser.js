import React, { Component } from 'react';
import { Button, Title } from 'react-native-paper';
import { StyleSheet, View, KeyboardAvoidingView, Platform, Image, Dimensions} from 'react-native';
import Header from '../components/Header';
import CustomSnackbar from '../components/CustomSnackbar';
import Input from '../components/Input';
import * as EmailValidator from 'email-validator';
import { validate } from 'gerador-validador-cpf'
import AsyncStorage from '@react-native-community/async-storage';
import Axios from '../commom/Axios';

export default class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            ddd: '',
            phone: '',
            cpf: '',
            pass: '',
            confpass: '',
            loadingButton: false
        };
        this.snacks = {}
    }

    handleSend() {
        this.snacks['success'].show()
    }

    async handleRegister() {

        await this.setState({ loadingButton: true })
        if(this.state.name == '') {
            await this.setState({ errorStatus: 'Campo nome requirido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(this.state.email == '') {
            await this.setState({ errorStatus: 'Campo email requirido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(EmailValidator.validate(this.state.email) == false) {
            await this.setState({ errorStatus: 'Formato de email inválido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(this.state.cpf.length < 11) {
            await this.setState({ errorStatus: 'CPF inválido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(validate(this.state.cpf) == false) {
            await this.setState({ errorStatus: 'CPF inválido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(this.state.ddd == '' || this.state.phone == '') {
            await this.setState({ errorStatus: 'Campo ddd+telefone requirido.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        if(this.state.pass == '' || this.state.pass != this.state.confpass) {
            await this.setState({ errorStatus: 'Senhas não correspondentes.', loadingButton: false})
            this.snacks['errors'].show();
            return;
        }

        let data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.ddd + this.state.phone,
            password: this.state.pass,
            cpf: this.state.cpf
        };

        let res = await Axios.newUser('user', data, async (error) => {
            console.error(error)
            await this.setState({ errorStatus: 'Houve um problema no servidor.', loadingButton: false});
            this.snacks['errors'].show();
        });

        if(res.status == 200)
            this.snacks['success'].show();

    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Header 
                    title="Cadastro" 
                    goBack={() => this.props.navigation.navigate('Login')} 
                />

                <KeyboardAvoidingView
                    behavior="padding"
                    style={styles.root}
                >

                    <View style={styles.loginContent}>
                        
                        <View style={styles.card}>
                            <Title>Informações Pessoais</Title>
                            <Input 
                                dense={true}
                                mode='outlined'
                                placeholder='Nome'
                                value={this.state.name}
                                onChangeText={name => this.setState({ name })}
                            />
                            <Input
                                dense 
                                keyboardType="email-address"
                                style={styles.textInput}
                                mode='outlined'
                                placeholder='Email'
                                value={this.state.email}
                                onChangeText={email => this.setState({ email })}
                            />
                            <Input
                                dense
                                keyboardType={Platform.OS == 'ios' ? 'number-pad' : "numeric"}
                                style={styles.textInput}
                                mode='outlined'
                                placeholder='CPF'
                                value={this.state.cpf}
                                onChangeText={cpf => {
                                    if(cpf.length <= 11)
                                        this.setState({ cpf })
                                }}
                            />
                            <View style={{flexDirection: 'row' }}>
                                <View style={{ flex: 1, marginRight: 5 }}>
                                    <Input 
                                        dense
                                        keyboardType={Platform.OS == 'ios' ? 'number-pad' : "numeric"}
                                        style={styles.textInput}
                                        mode='outlined'
                                        placeholder='DDD'
                                        value={this.state.ddd}
                                        onChangeText={ddd => {
                                            if(ddd.length <= 2)
                                                this.setState({ ddd })
                                        }}
                                    />
                                </View>
                                <View style={{ flex: 2 }}>
                                    <Input 
                                        dense
                                        maxLength={9}
                                        keyboardType={Platform.OS == 'ios' ? 'number-pad' : "numeric"}
                                        style={styles.textInput}
                                        mode='outlined'
                                        placeholder='Telefone'
                                        value={this.state.phone}
                                        onChangeText={phone => {
                                            if(phone.length <= 9)
                                                this.setState({ phone })
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.card}>
                            <Title>Senha</Title>
                            <Input
                                dense 
                                secureTextEntry={true}
                                style={styles.textInput}
                                mode='outlined'
                                placeholder='Senha'
                                value={this.state.pass}
                                onChangeText={pass => this.setState({ pass })}
                            />
                            <Input
                                dense
                                secureTextEntry={true} 
                                style={styles.textInput}
                                mode='outlined'
                                placeholder='Confirmar Senha'
                                value={this.state.confpass}
                                onChangeText={confpass => this.setState({ confpass })}
                            />
                        </View>
                    </View>

                    <View style={styles.buttonContent}>
                        <Button 
                            onPress={() => this.handleRegister()}
                            contentStyle={styles.button} 
                            mode="contained"
                            loading={this.state.loadingButton}
                        >
                            Finalizar Cadastro
                        </Button>
                    </View>
                </KeyboardAvoidingView>

                <CustomSnackbar 
                    onRef={(ref)=> { this.snacks['success'] = ref }} 
                    text="Cadastro Realizado!" 
                    onClose={ async () => {

                        // Logando (pegando token)
                        let data = {
                            'username': this.state.email,
                            'password': this.state.pass
                        };

                        let config = {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        };

                        let token = await Axios.token(this.state.email, this.state.pass, async (error) => {
                            console.error(error)
                            await this.setState({ errorStatus: 'Houve um problema no servidor.'});
                            this.snacks['errors'].show();
                        });
                        
                        // Salvando token no cache
                        await AsyncStorage.setItem('user_token', token); 
            
                        let user = await Axios.get('me', token);

                        await AsyncStorage.setItem('pass', this.state.pass);
                        await AsyncStorage.setItem('user', JSON.stringify(user.data));

                        // Verificando de qual tela veio
                        let sub_screen = await AsyncStorage.getItem('sub_screen');
                        await AsyncStorage.setItem('sub_screen', 'NONE');

                        if(sub_screen !== 'NONE')
                            this.props.navigation.navigate('UserAddresses');
                        else{
                            let screen = await AsyncStorage.getItem('screen');
                            await AsyncStorage.setItem('screen', 'NONE');
                            if(screen == 'PROFILE')
                                this.props.navigation.navigate('Profile');
                            else if (screen == 'HISTORIC')
                                this.props.navigation.navigate('Historic');
                            else if (screen == 'HELP_REQUEST') {
                                await AsyncStorage.setItem('screen', 'LOGIN');
                                this.props.navigation.navigate('MoreInfoHelpRequest');
                            }else if (screen == 'HELP_OFFER') {
                                await AsyncStorage.setItem('screen', 'LOGIN');
                                this.props.navigation.navigate('MoreInfoHelpOffer'); 
                            }else if(screen == 'HELP_REQ_APPLY') {
                                await AsyncStorage.setItem('screen', 'LOGIN');
                                this.props.navigation.navigate("MoreInfoHelpRequestView");
                            }else if (screen == 'APPLY_OFFER') {
                                await AsyncStorage.setItem('screen', 'LOGIN');
                                this.props.navigation.navigate('MoreInfoApplyOffer');
                            }
                        }
                    }}
                />

                <CustomSnackbar
                    error={true}
                    onRef={(ref) => { this.snacks['errors'] = ref}}
                    text={this.state.errorStatus}
                />

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flex: 3
    },
    text: {
        textAlign: 'justify'
    },
    importantText: {
        fontWeight: 'bold'
    },
    loginContent: {
        marginTop: 15,
        flex: 9
    },
    buttonContent: {
        paddingVertical: 10,
        flex: 1,
    },
    button: {
        paddingVertical: 5,
    },
    hideButton: {
        letterSpacing: 0,
    },
    card: {
        paddingVertical: 10
    }

})