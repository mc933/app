import React, { Component } from 'react';
import { Button, Subheading } from 'react-native-paper';
import { StyleSheet, View, Platform, StatusBar, Image, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TabView, SceneMap } from 'react-native-tab-view';
import InProgress from './InProgress';
import Finished from './Finished';

export default class Historic extends Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [{
                key: 'first', title: 'Em progresso',
            }, {
                key: 'second', title: 'Finalizadas'
            }],
            user: {},
            logged: false
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyLogin();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    async verifyLogin() {
        let logged = false;
        let token = await AsyncStorage.getItem('user_token');

        if(token !== 'FALSE') {
            logged = true;
        
            // Pegando objeto do usuário
            let user = JSON.parse(await AsyncStorage.getItem('user'));

            this.setState({ logged, user });
        }else
            this.setState({ logged });
    }

    render() {

        const inProgress = () => (
            <InProgress 
                askList={this.state.user.asked_helps} 
                offerList={this.state.user.offered_helps} 
                navigation={this.props.navigation} 
            />
        );

        const finished = () => (
            <Finished 
                askList={this.state.user.asked_helps} 
                offerList={this.state.user.offered_helps}  
            />
        );

        const renderScene = SceneMap({
            first: inProgress,
            second: finished
        });

        return (
            <View style={{ flex: 1, paddingTop: Platform.SO == 'ios' ? 44 : StatusBar.currentHeight, backgroundColor: '#fff' }}>

                { this.state.logged
                    ? <TabView 
                        navigationState={{ index: this.state.index, routes: this.state.routes }}
                        renderScene={renderScene}
                        onIndexChange={index => this.setState({ index })}
                    />
                    : <View style={{ flex: 1, alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Subheading>Lembrete: Lave bem as mãos</Subheading>
                        <Image
                            style={{ width: Dimensions.get('window').width*0.6, height: Dimensions.get('window').height*0.3 }}
                            source={require('../assets/images/login_historic.jpg')}
                        />
                        <Subheading>Faça login para ver seu perfil</Subheading>
                        <Button 
                            mode='contained' 
                            style={{ marginVertical: 10 }}
                            onPress={ async () => {
                                await AsyncStorage.setItem('screen', 'HISTORIC');
                                this.props.navigation.navigate('Login')
                            }}
                        >
                            LOGIN
                        </Button>
                    </View>
                }

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 20,
        flex: 1
    },
    loginContent: {
        marginTop: 15,
        flex: 1
    },
    buttonContent: {
        paddingVertical: 10,
        flex: 1
    },
    button: {
        paddingVertical: 10,
    },
    card: {
        paddingVertical: 10
    },
    avatar: {
        borderWidth: 2, borderColor: 'rgba(64, 68, 76, 1)',
    },

})