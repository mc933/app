import React, { Component } from 'react';
import { TextInput, Caption, Button, Title } from 'react-native-paper';
import { StyleSheet, View, KeyboardAvoidingView, Platform, Image, Dimensions, Animated, Keyboard } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from '../commom/Axios';
import CustomSnackbar from '../components/CustomSnackbar';

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: '',
            pass: '',
            loadingButton: false
        };

        this.snacks = {};
    }

    async handleLogin() {

        await this.setState({ loadingButton: true });

        let token = await Axios.token(this.state.login, this.state.pass, async (error) => {
            console.log(error)
            await this.setState({ errorStatus: 'Houve um problema no servidor.', loadingButton: false});
            this.snacks['errors'].show();
            return;
        });

        // Salvando token no cache
        await AsyncStorage.setItem('user_token', token); 
        
        // Salvando objeto usuário no localStorage
        let user = await Axios.get('me', token);

        await AsyncStorage.setItem('pass', this.state.pass);
        await AsyncStorage.setItem('user', JSON.stringify(user.data));

        // Verificando de qual tela veio
        let sub_screen = await AsyncStorage.getItem('sub_screen');
        await AsyncStorage.setItem('sub_screen', 'NONE');

        if(sub_screen !== 'NONE')
            this.props.navigation.navigate('UserAddresses');
        else{
            let screen = await AsyncStorage.getItem('screen');
            await AsyncStorage.setItem('screen', 'NONE');
            if(screen == 'PROFILE')
                this.props.navigation.navigate('Profile');
            else if (screen == 'HISTORIC')
                this.props.navigation.navigate('Historic');
            else if (screen == 'HELP_REQUEST') {
                await AsyncStorage.setItem('screen', 'LOGIN');
                this.props.navigation.navigate('MoreInfoHelpRequest');
            }else if (screen == 'HELP_OFFER') {
                await AsyncStorage.setItem('screen', 'LOGIN');
                this.props.navigation.navigate('MoreInfoHelpOffer'); 
            }else if(screen == 'HELP_REQ_APPLY') {
                await AsyncStorage.setItem('screen', 'LOGIN');
                this.props.navigation.navigate("MoreInfoHelpRequestView");
            }else if (screen == 'APPLY_OFFER') {
                await AsyncStorage.setItem('screen', 'LOGIN');
                this.props.navigation.navigate('MoreInfoApplyOffer');
            }
        }
    }

    render() {
        return (
                    
            <View
                style={{ flex: 1, backgroundColor: '#fff' }}
            >
                <View style={styles.title}>
                    <Image
                        style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height*0.3 }}
                        source={require('../assets/images/logo.jpg')}
                    />
                    <Title style={{
                        color: '#2d75e4',
                        fontSize: 42,
                        textAlign: 'left',
                        paddingTop: 45,
                        paddingHorizontal: 50,
                        textShadowOffset: { width: 3, height: 3 },
                        textShadowColor: '#d6d6d6',
                        textShadowRadius: 2,
                    }}>
                        Isolamento</Title>
                    <Title style={{
                        color: '#2d75e4',
                        fontSize: 42,
                        textAlign: 'right',
                        paddingTop: 15,
                        paddingHorizontal: 50,
                        textShadowOffset: { width: 3, height: 3 },
                        textShadowColor: '#d6d6d6',
                        textShadowRadius: 2,
                    }}>
                        Solidário</Title>
                </View>

                <View style={styles.loginContent}>
                    <TextInput 
                        style={styles.textInput}
                        placeholder='Usuário'
                        mode='outlined'
                        keyboardType="email-address"
                        autoCapitalize="none"
                        value={this.state.login}
                        onChangeText={login => {this.setState({ login })}}
                    />
                    <TextInput 
                        style={styles.textInput}
                        secureTextEntry={true}
                        placeholder='Senha'
                        mode='outlined'
                        value={this.state.pass}
                        onChangeText={pass => {this.setState({ pass })}}
                    />
                    <View style={styles.buttonContent}>
                        <Button 
                            loading={this.state.loadingButton}
                            onPress={() => this.handleLogin()}
                            contentStyle={styles.button} 
                            mode="contained">Entrar</Button>
                    </View>
                </View>

                <View style={styles.newAccountContent}>
                    <Caption style={styles.caption}>Novo por aqui? </Caption>
                    <Caption
                        onPress={() => this.props.navigation.navigate("NewUser")} 
                        style={[styles.caption, styles.link]}>Crie uma conta!</Caption>
                </View>

                <CustomSnackbar
                    error={true}
                    onRef={(ref)=> { this.snacks['errors'] = ref }}
                    text={this.state.errorStatus}
                />

            </View>

        )
    }
}

const styles = StyleSheet.create({
    loginContent: {
        paddingHorizontal: 50,
        flex: 2
    },
    textInput: {
        backgroundColor: 'rgba(255,255,255,0.8)'
    },
    buttonContent: {
        paddingVertical: 5
    },
    button: {
        paddingVertical: 10,
    },
    newAccountContent: {
        alignSelf: 'center',
        flexDirection: 'row',
        marginTop: 'auto',
        paddingVertical: 25,
        flex: 1,
    },
    caption: {
        fontSize: 16
    },
    link: {
        fontWeight: 'bold',
    },
    title: {
        flex: 3,
        paddingVertical: 25,
        alignSelf: 'center',
    }
})