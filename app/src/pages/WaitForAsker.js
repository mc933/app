import React, { Component } from 'react';
import { Button, List, Headline, Subheading, Avatar, Paragraph } from 'react-native-paper';
import { StyleSheet, View } from 'react-native';
import Header from '../components/Header';


export default class teste extends Component {
    constructor(props) {
        super(props);
        

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header
                    openMenu={() => this.props.navigation.openDrawer()}
                    title="Aguardar um voluntário"
                />
                <View style={{marginTop: 5, borderWidth: 1,paddingVertical:20, paddingHorizontal:120, alignSelf:"center"}}>
                    <Headline>Mercado</Headline>
                    <Subheading>Data</Subheading>
                    <Subheading>Horário</Subheading>
                </View>
                <View style={{marginTop: 5,paddingVertical:20, paddingHorizontal: 30, alignSelf:"center"}}>
                    <Headline>Obrigada por oferecer ajuda!</Headline>
                    <Paragraph>Sua ajuda constará na lista de ajudas para pessoas vulneráveis!</Paragraph>
                    <Paragraph>Vamos te notificar assim que alguém precisar da sua ajuda!</Paragraph>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 20
    },
    cardContent: {
        paddingVertical: 25
    },
    loginContent: {
        marginTop: 100
    },
})