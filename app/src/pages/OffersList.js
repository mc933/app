import React, { Component } from 'react';
import { Subheading, Button, Caption, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import Loader from '../components/Loader';
import * as Location from 'expo-location';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from '../commom/Axios';
import moment from 'moment';
import { getDistance } from 'geolib'

export default class OffersList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            locationResult: {},
            errorMessage: '',
            offersList: []
        };

        this.loader = React.createRef();
    }

    async componentDidMount() {

        // Seta o loading
        await this.setState({ loading: true });
        this.loader.show();

        // Verifica a permissão da localização
        let { status } = await Location.getPermissionsAsync();

        if(status !== 'granted') {
            let { status: finalStatus } = await Location.requestPermissionsAsync();

            if(finalStatus !== 'granted') {
                this.loader.close();
                this.setState({ errorMessage: 'PERMISSÃO NÃO GARANTIDA', loading: false });
                return;
            }
        }

        let locationResult = {};
        let coordUser = {};

        // Verifica a cache
        let address = await AsyncStorage.getItem('address');
 
        // Se tiver endereço na cache
        if(address !== 'NONE') {
            locationResult = JSON.parse(address);
            coordUser = { latitude: locationResult.lat, longitude: locationResult.long }
        }else{
            let gpsAddress = await AsyncStorage.getItem('GPSAddress');
            if(gpsAddress !== 'NONE') {
                locationResult = JSON.parse(gpsAddress);
                coordUser = { latitude: locationResult.lat, longitude: locationResult.long }
            }else{

                const location = await Location.getCurrentPositionAsync({
                    accuracy: Location.Accuracy.Highest,
                });

                coordUser = {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                };

                let res = await Axios.getAddressByCoordinate(coordUser, (error) => {
                    console.log(error);
                    this.loader.close();
                    this.setState({ loading: false })
                });

                locationResult = {
                    lat: coordUser.latitude,
                    long: coordUser.longitude,
                    city: res.data.data[0].city,
                    street: res.data.data[0].street,
                    number: res.data.data[0].number,
                }
    
                await AsyncStorage.setItem('GPSAddress', JSON.stringify(locationResult));
            }
        }

        // Verificando se usuário está logado
        let logged = false;
        let user = await AsyncStorage.getItem('user');
        if(user !== 'NONE') {
            logged = true;
            user = JSON.parse(user);
        }

        // Pegar os oferecimentos de ajuda
        let res = await Axios.get('help_offer');
        let helps = [];

        for(let i = 0; i < res.data.length; i++) {
            
            // Verificando a distancia do usuário
            let coordHelp = { latitude: res.data[i].lat ? res.data[i].lat : 0, longitude: res.data[i].long ? res.data[i].long : 0 };

            let distance = getDistance(coordUser, coordHelp);

            // Verificando se é o mesmo usuário
            //let sameUser = false;
            let sameUser = logged && user.id != res.data[i].offering_user_id;

            // Verificando se já ajuda já foi ocupada
            //let busy = false;
            let busy = res.data[i].requests.length > 0

            if(distance <= 10000 && !sameUser && !busy) {
                let resUser = await Axios.get('user/' + res.data[i].offering_user_id.toString());
                res.data[i].user = resUser.data;
                helps.push(res.data[i]);
            }
        }

        this.loader.close();
        this.setState({ locationResult, loading: false, offersList: helps });

    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }}>                        

                    { !this.state.loading
                        ? <View style={styles.root}>
                            <View style={styles.cardContent}>

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Subheading style={styles.importantText}>
                                        {this.state.locationResult.street}{this.state.locationResult.number ? ', ' + this.state.locationResult.number : null}
                                    </Subheading>
                                    <IconButton 
                                        icon='chevron-down' 
                                        color={'#2d75e4'}
                                        style={{ margin: 0, marginRight: 'auto' }} 
                                        onPress={async () => {
                                            await AsyncStorage.setItem('screen', 'OFFER_LIST')
                                            this.props.navigation.navigate('UserAddresses')
                                        }}
                                    />
                                </View>

                                <View>
                                    <Subheading style={styles.importantText}>
                                        Não achou a ajuda que queria? 
                                    </Subheading>
                                    <Button 
                                        onPress={() => this.props.navigation.navigate('HelpType')}
                                        mode="contained">
                                            PEDIR NOVA AJUDA
                                        </Button>
                                </View>

                                <FlatList 
                                    style={{ marginVertical: 10 }}
                                    ListHeaderComponent={<Subheading style={styles.importantText}>Ajudas oferecidas próximas à você</Subheading>}
                                    ListEmptyComponent={
                                        <View style={{ alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                            <IconButton icon="cart-outline" size={100} color="rgba(0,0,0,0.2)" />
                                            <Subheading>Nenhuma ajuda oferecida perto de você :(</Subheading>
                                        </View>
                                    }
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.offersList}
                                    keyExtractor={item => item.id.toString()}
                                    renderItem={({ item }) => (
                                        <TouchableOpacity style={styles.itemContent} onPress={() => this.props.navigation.navigate('TodoistApplyOffer', { help: item })}>
                                            <View>
                                                <Subheading>{item.type} ({moment(new Date(item.help_day)).format('DD/MM')} às {moment(new Date(item.hour)).format('HH:mm')}h)</Subheading>
                                                <Caption>Oferecida por: {item.user.name}</Caption>
                                                <Caption>Preço máximo: R$ {item.max_cost}</Caption>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                />
                            </View>
                        </View>
                        : <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}> 
                        <Loader ref={ref=>this.loader=ref}/>
                        </View>
                    }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        flex: 1,
        borderWidth: 2,
        marginVertical: 10,
        padding: 10,
        borderRadius: 5,
        borderColor: 'rgba(64, 68, 76, 0.3)'
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    }
})