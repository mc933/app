import React, { Component } from 'react';
import { Button, List, Headline, Subheading, Avatar, Paragraph } from 'react-native-paper';
import { StyleSheet, View } from 'react-native';
import Header from '../components/Header';


export default class teste extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     name,
        //     neighborhood,
        //     foto,
        //     phoneNumber
        // }

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header
                    openMenu={() => this.props.navigation.openDrawer()}
                    title="Aguardar um voluntário"
                />
                <View style={{marginTop: 5, borderWidth: 1,paddingVertical:20, paddingHorizontal:120, alignSelf:"center"}}>
                    <Headline>Mercado</Headline>
                    <Subheading>Data</Subheading>
                    <Subheading>Horário</Subheading>
                </View>
                <View style={{marginTop: 5,paddingVertical:20, paddingHorizontal: 30, alignSelf:"center"}}>
                    <Headline>Obrigada por fazer o seu pedido!</Headline>
                    <Paragraph>Seu pedido constará na lista de pedidos para os voluntários! Esperamos que em breve ele seja atendido!</Paragraph>
                    <Paragraph>Se precisar de mais alguma compra, é só pedir uma nova ajuda!</Paragraph>
                </View>

                <View style={styles.bottom}>
                    <Button
                        onPress={() => this.props.navigation.navigate('Home')}
                        contentStyle={{ paddingHorizontal: 25 }}
                        mode="contained">
                    Voltar para a Home
                    </Button>

                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 20
    },
    cardContent: {
        paddingVertical: 25
    },
    loginContent: {
        marginTop: 100
    },
    bottom: {
        position: 'absolute',
        left:50,
        right:50,
        bottom: 50
    },
})