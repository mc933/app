import React, { Component } from 'react';
import { Subheading, Button, Caption, IconButton } from 'react-native-paper';
import { StyleSheet, View, Platform, StatusBar } from 'react-native';
import DateTimePicker from '../components/DateTimePicker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Input from '../components/Input';
import Axios from '../commom/Axios';
import CustomSnackbar from '../components/CustomSnackbar';

export default class MoreInfoHelpOffer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            address: {},
            logged: false,
            lat: 0,
            long: 0,
            help_day: new Date(),
            hour: new Date(),
            notes: '',
            type: '',
            max_cost: ''
        };

        this.addressLoader = React.createRef();
        this.snacks = {}
    }

    async verifyRoute() {
        console.log('\n MORE INFO ');

        // Verificando se está logado
        let logged = false;
        let token = await AsyncStorage.getItem('user_token');
        console.log('\nTOKEN: ', token);
        if(token !== 'FALSE')
            logged = true;
    
        // Pegando localização
        let address = JSON.parse(await AsyncStorage.getItem('GPSAddress'));
        let currentAddress = await AsyncStorage.getItem('address');
        if(currentAddress !== 'NONE')
            address = JSON.parse(currentAddress)

        // Verificando de que rota veio para pegar os parametros
        let helpOffer = {};
        let screen = await AsyncStorage.getItem('screen');
        if(screen == 'LOGIN' || screen == 'ADDRESS') {
            helpOffer = JSON.parse(await AsyncStorage.getItem('help_offer'));
        }else{
            helpOffer = {
                lat: address.lat,
                long: address.long,
                help_day: new Date(),
                hour: new Date(),
                notes: '',
                max_cost: '',
                type: this.props.route.params.type,
            };
        }

        this.setState({ 
            logged, 
            address,
            lat: helpOffer.lat,
            long: helpOffer.long,
            help_day: helpOffer.help_day,
            hour: helpOffer.hour,
            notes: helpOffer.notes,
            type: helpOffer.type,
            max_cost: helpOffer.max_cost
        });
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyRoute();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>                      
                <View style={[styles.root, { marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }]}>

                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <Subheading style={styles.importantText}>Seu endereço</Subheading>
                            <TouchableOpacity style={styles.cardItem}
                                onPress={async () => {

                                    let helpOffer = {
                                        max_cost: this.state.max_cost,
                                        notes: this.state.notes,
                                        lat: this.state.address.lat,
                                        long: this.state.address.long,
                                        help_day: this.state.help_day,
                                        hour: this.state.hour,
                                        type: this.state.type,
                                    };

                                    await AsyncStorage.setItem('help_offer', JSON.stringify(helpOffer));

                                    await AsyncStorage.setItem('screen', 'HELP_OFFER');
                                    this.props.navigation.navigate('UserAddresses')
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    <IconButton style={{ margin: 0, alignSelf: 'center' }} icon="crosshairs-gps" />
                                    <View style={{ paddingHorizontal: 10 }}>
                                        <Subheading>
                                            {this.state.address.street}{this.state.address.number !== null ? ', ' + this.state.address.number : null}
                                        </Subheading>
                                        <Caption>
                                            {this.state.address.city !== 'NULL' ? this.state.address.city : null} 
                                        </Caption>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Subheading style={styles.importantText}>Data e Hora que irá para {this.state.type}</Subheading>
                            <View style={{ flexDirection: 'row', marginVertical: 5, alignContent: 'center', alignItems: 'center' }}>
                                <Subheading>Dia  </Subheading>
                                <DateTimePicker 
                                    mode={'date'}
                                    value={this.state.help_day}
                                    onChange={(event, selectedDate) => this.setState({ help_day: selectedDate })}
                                />
                                <Subheading>  a partir das  </Subheading>
                                <DateTimePicker 
                                    mode={'time'}
                                    value={this.state.min_available_hour}
                                    onChange={(event, selectedDate) => this.setState({ min_available_hour: selectedDate })}
                                />
                            </View>
                        </View>

                        <View style={{ flex: 1}}>
                            <Input 
                                mode='outlined'
                                placeholder='Valor máximo'
                                value={this.state.max_cost}
                                onChangeText={max_cost => this.setState({ max_cost })}
                            />
                        </View>

                        <View style={{ flex: 1}}>
                            <Input 
                                mode='outlined'
                                placeholder='Quer fazer alguma observação?'
                                value={this.state.notes}
                                onChangeText={notes => this.setState({ notes })}
                            />
                        </View>

                        <View style={{ flex: 1, marginTop: 20 }}>

                            {this.state.logged
                                ? <Button mode="contained"
                                    onPress={ async () => {
                                        let token = await AsyncStorage.getItem('user_token');
                                        
                                        let data = { 
                                            max_cost: parseFloat(this.state.max_cost),
                                            notes: this.state.notes,
                                            lat: this.state.address.lat,
                                            long: this.state.address.long,
                                            help_day: this.state.help_day.toString(),
                                            hour: this.state.hour.toString(),
                                            type: this.state.type,
                                        };

                                        let res = await Axios.post('help_offer', data, token);
                                        
                                        if(res.status == 200)
                                            this.snacks['success'].show();

                                    }}
                                >
                                    Finalizar
                                </Button>
                                : <Button mode="contained"
                                    onPress={async () => {

                                        let helpOffer = {
                                            max_cost: this.state.max_cost,
                                            notes: this.state.notes,
                                            lat: this.state.address.lat,
                                            long: this.state.address.long,
                                            help_day: this.state.help_day,
                                            min_available_hour: this.state.min_available_hour,
                                            max_available_hour: this.state.max_available_hour,
                                            type: this.state.type,
                                            items: this.state.items
                                        };

                                        await AsyncStorage.setItem('help_offer', JSON.stringify(helpOffer));

                                        await AsyncStorage.setItem('screen', 'HELP_OFFER');
                                        this.props.navigation.navigate('Login')
                                    }}
                                >
                                    Faça login para finalizar
                                </Button>
                            }

                            <Button 
                                mode="contained" 
                                style={{ marginTop: 10 }} 
                                onPress={() => this.props.navigation.goBack()}
                            > 
                                Voltar
                            </Button>
                        </View>
                    </View>
                </View>

                <CustomSnackbar
                    onRef={(ref) => { this.snacks['success'] = ref}}
                    text={"Oferecimento de ajuda criado com sucesso, aguarde algum usuário requisitá-lo!"}
                    onClose={async () =>{
                        await AsyncStorage.setItem('screen', 'NONE');
                        await AsyncStorage.setItem('help_offer', 'NONE');
                        this.props.navigation.pop();
                        this.props.navigation.pop();
                        this.props.navigation.pop();
                        this.props.navigation.push("AskersList");
                    } }
                />

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        borderRadius: 5,
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.6)',
        padding: 10,
        marginVertical: 5
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },

    inUseCardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgb(45, 117, 228)', padding: 10, marginVertical: 5 },
    cardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgba(0,0,0,0.6)', padding: 10, marginVertical: 5 }
})