import React, { Component } from 'react';
import { Headline, Subheading, Button, TextInput, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import CustomSnackbar from '../components/CustomSnackbar';
import Header from '../components/Header';
import AsyncStorage from '@react-native-community/async-storage';

export default class TodoistApplyOffer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            todoist: []
        }

        this.baseState = this.state;
        this.snacks = {}
    }

    resetComponent = () => {
        this.setState(this.baseState)
    }

    render() {

        const { help } = this.props.route.params;

        let totalValue = 0;
        for(let i = 0; i < this.state.todoist.length; i++) {
            if(this.state.todoist[i].max_cost !== '')
                totalValue+=parseFloat(this.state.todoist[i].max_cost)*this.state.todoist[i].quantity;
        };

        return (
            <View style={{ flex: 1, marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }}>                      

                <View style={styles.root}>
                    <View style={styles.cardContent}>
                        <FlatList 
                            ListHeaderComponent={
                                <View style={{ paddingVertical: 20 }}>
                                    <Subheading style={[styles.title, { textAlign: 'left' }]}>
                                        Faça sua lista de compras para {help.type}.
                                    </Subheading>
                                    <Subheading style={[styles.title, { textAlign: 'justify', fontWeight: 'normal'}]}>
                                        Para cada item preencha nome, valor máximo que quer pagar e a quantidade.
                                    </Subheading>
                                </View>
                            }
                            ListFooterComponent={
                                <View style={{ marginTop: 10 }}>
                                    <Button 
                                        mode="contained" 
                                        onPress={() => {
                                            let data = JSON.parse(JSON.stringify(this.state.todoist))
                                            data.push({ name: '', quantity: 1, max_cost: '' })
                                            this.setState({ todoist: data });
                                        }}
                                    >
                                        Adicionar Item
                                    </Button>
                                </View>
                            }
                            showsVerticalScrollIndicator={false}
                            data={this.state.todoist}
                            keyExtractor={(item, index) => item.name.toString()+index}
                            renderItem={({ item, index }) => (
                                <View style={{ flexDirection: 'row' }}>
                                    <TextInput 
                                        style={{ width: '40%' }}
                                        dense={true}
                                        mode='outlined'
                                        placeholder={'Item ' + index}
                                        value={item.name}
                                        onChangeText={name => {
                                            let data = JSON.parse(JSON.stringify(this.state.todoist))
                                            data[index].name = name;
                                            this.setState({ todoist: data })
                                        }}
                                    />

                                    <TextInput 
                                        style={{ width: '15%', marginHorizontal: '2.5%' }}
                                        dense={true}
                                        mode='outlined'
                                        placeholder={'R$'}
                                        value={item.max_cost}
                                        keyboardType='numeric'
                                        onChangeText={max_cost => {
                                            let data = JSON.parse(JSON.stringify(this.state.todoist))
                                            data[index].max_cost = max_cost;
                                            this.setState({ todoist: data })
                                        }}
                                    />

                                    <View style={{ width: '25%', marginRight: '2.5%', flexDirection: 'row', alignItems: 'center' }}>
                                        <IconButton 
                                            icon="minus-circle-outline" 
                                            onPress={() => {
                                                let data = JSON.parse(JSON.stringify(this.state.todoist))
                                                if(data[index].quantity > 0) {
                                                    data[index].quantity -= 1;
                                                    this.setState({ todoist: data })
                                                }
                                            }}
                                        />
                                        <Headline>{item.quantity}</Headline>
                                        <IconButton 
                                            icon="plus-circle-outline" 
                                            onPress={() => {
                                                let data = JSON.parse(JSON.stringify(this.state.todoist))   
                                                data[index].quantity += 1;
                                                this.setState({ todoist: data })
                                            }}
                                        />
                                    </View>

                                        { index > 0 && 
                                            <IconButton 
                                                style={{ width: '10%' }} 
                                                icon="close-circle" 
                                                onPress={() => {
                                                    let data = JSON.parse(JSON.stringify(this.state.todoist))
                                                    data.splice(index, 1);   
                                                    this.setState({ todoist: data })   
                                                }}
                                            /> 
                                        }
                                </View>
                            )}
                        />

                        <View style={{ marginVertical: 20 }}>

                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Subheading style={{ fontWeight: 'bold', marginRight: 'auto', color: help.max_cost < totalValue ? 'red' : 'black' }}>
                                    Valor Total: R$ { totalValue }
                                </Subheading>
                                <Subheading style={{ fontWeight: 'bold', marginLeft: 'auto' }}>
                                    Valor Máximo: R$ { help.max_cost }
                                </Subheading>
                            </View>

                            <Button 
                                mode="contained" 
                                onPress={async () => {

                                    // Verificando se tem algum campo vazio
                                    if(this.state.todoist.length  == 0) {
                                        await this.setState({ errorStatus: 'Adicione no mínimo 1 item'});
                                        this.snacks['errors'].show();
                                        return;
                                    }

                                    if(help.max_cost < totalValue) {
                                        await this.setState({ errorStatus: 'Valor máximo ultrapassado, remova alguns items!'});
                                        this.snacks['errors'].show();
                                        return;
                                    }

                                    for(let i = 0; i < this.state.todoist.length; i++) {
                                        let item = this.state.todoist[i];

                                        if(item.name == '' || item.quantity == 0 || item.max_cost == '') {
                                            await this.setState({ errorStatus: 'Preencha todos os campos de todos os itens.'})
                                            this.snacks['errors'].show();
                                            return;
                                        }else if(item.max_cost == 0) {
                                            await this.setState({ errorStatus: 'Coloque um valor maior que 0 para o preço máximo de cada item.'})
                                            this.snacks['errors'].show();
                                            return;
                                        }
                                    }

                                    await AsyncStorage.setItem('screen', 'APPLY_OFFER')
                                    this.props.navigation.navigate('MoreInfoApplyOffer', { items: this.state.todoist, help: help, max_cost: totalValue })
                                }}
                            >
                                Próximo
                            </Button>

                            <Button mode="contained" style={{ marginTop: 10 }} onPress={() => this.props.navigation.goBack()}> 
                                Voltar
                            </Button>
                        </View>

                        <CustomSnackbar
                            error={true}
                            onRef={(ref) => { this.snacks['errors'] = ref}}
                            text={this.state.errorStatus}
                        />
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        flex: 1,
        borderWidth: 1,
        marginVertical: 10,
        padding: 10,
        borderBottomWidth: 5,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        borderRightWidth: 5,
        borderRightColor: 'rgba(0,0,0,0.1)',
        borderRadius: 10,
        borderTopWidth: 1,
        borderTopColor: 'rgba(0,0,0,0.1)',
        borderLeftWidth: 1,
        borderLeftColor: 'rgba(0,0,0,0.1)',
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },
    title: {
        //color: '#000',
        //fontWeight: 'bold',
        //marginTop: 30,
        color: 'rgba(64, 68, 76, 1)',
        //fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold'
    }
})