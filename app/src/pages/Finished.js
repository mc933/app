import React, { Component } from 'react';
import { Subheading, Caption, Divider, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import Loader from '../components/Loader';
import { TouchableOpacity } from 'react-native-gesture-handler';
import moment from 'moment';
import {FontAwesome5} from '@expo/vector-icons';

export default class Finished extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            askList: [],
            offerList: []
        };

        this.loader = React.createRef();
    }

    async componentDidMount() {

        // Seta o loading
        await this.setState({ loading: true });
        this.loader.show();

        let askList = [];
        let offerList = [];

        for(let i = 0; i < this.props.askList.length; i++) {
            if(this.props.askList[i].fulfilled != false)
                askList.push(this.props.askList[i]);
        }

        for(let i = 0; i < this.props.offerList.length; i++) {
            if(this.props.offerList[i].finished != false)
                offerList.push(this.props.offerList[i]);
        }

        this.loader.close();
        this.setState({ askList, loading: false, offerList });

    }

    render() {
        return (
            <View style={{ flex: 1, paddingTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }}>                        

                    { !this.state.loading
                        ? <View style={styles.root}>
                            <View style={styles.cardContent}>

                                <FlatList
                                    ListEmptyComponent={
                                        <View style={{ alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                            <IconButton icon="cart-outline" size={100} color="rgba(0,0,0,0.2)" />
                                            <Subheading style={{ textAlign: 'center' }}>Você não possui nenhum pedido de ajuda completo!</Subheading>
                                        </View>
                                    } 
                                    style={{ flex: 1, paddingBottom: 10 }}
                                    ListHeaderComponent={<Subheading style={styles.importantText}>Pedidos de Ajuda</Subheading>}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.askList}
                                    keyExtractor={item => item.id.toString()}
                                    renderItem={({ item }) => {
                                        let hourMin = new Date(item.min_available_hour);
                                        hourMin = moment(hourMin).format('HH:mm');

                                        let hourMax = new Date(item.max_available_hour);
                                        hourMax = moment(hourMax).format('HH:mm');

                                        let date = new Date(item.help_day);
                                        date = moment(date).format('DD/MM');
    
                                        return(
                                            <View style={styles.itemContent}>
                                                <View>
                                                    <Subheading>{item.type} - {date}</Subheading>
                                                    {/*<Caption>Pedido por {item.user.name}</Caption>*/}
                                                    <Caption>Entrega das {hourMin}h até as {hourMax}h</Caption>
                                                    <Caption>Valor máximo: R$ {item.max_cost}</Caption>
                                                </View>
                                            </View>
                                        )
                                    }}
                                />

                                <Divider />

                                <FlatList
                                    ListEmptyComponent={
                                        <View style={{ alignContent:'center', alignItems: 'center', justifyContent: 'center', marginVertical: 30 }}>
                                            <FontAwesome5 name="hands-helping" color='rgba(0,0,0,0.2)' size={100} />
                                            <Subheading style={{ marginTop: 30, textAlign: 'center' }}>Você não possui oferecimento de ajuda completo!</Subheading>
                                        </View>
                                    } 
                                    style={{ flex: 1, paddingTop: 10 }}
                                    ListHeaderComponent={<Subheading style={styles.importantText}>Oferecimento de Ajuda</Subheading>}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.offerList}
                                    keyExtractor={item => item.id.toString()}
                                    renderItem={({ item }) => {
                                        let hour = new Date(item.hour);
                                        hour = moment(hour).format('HH:mm');

                                        let date = new Date(item.help_day);
                                        date = moment(date).format('DD/MM');
    
                                        return(
                                            <View style={styles.itemContent}>
                                                <View>
                                                    <Subheading>{item.type} - {date}</Subheading>
                                                    {/*<Caption>Pedido por {item.user.name}</Caption>*/}
                                                    <Caption>Entrega apartir das {hour}h</Caption>
                                                    <Caption>Valor máximo: R$ {item.max_cost}</Caption>
                                                </View>
                                            </View>
                                        )
                                    }}
                                />
                            </View>
                        </View>
                        : <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}> 
                        <Loader ref={ref=>this.loader=ref}/>
                        </View>
                    }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
    },
    itemContent: {
        flex: 1,
        borderWidth: 2,
        marginVertical: 10,
        padding: 10,
        borderRadius: 5,
        borderColor: 'rgba(64, 68, 76, 0.3)'
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    }
})