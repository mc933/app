import React, { Component } from 'react';
import { Subheading, Button, Caption, IconButton, Divider, Portal, Dialog } from 'react-native-paper';
import { StyleSheet, View, FlatList } from 'react-native';
import Header from '../components/Header';
import Loader from '../components/Loader';
import Input from '../components/Input';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import CustomSnackbar from '../components/CustomSnackbar';
import Axios from '../commom/Axios';

export default class UserAddresses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchAddress: '',
            searchAddresses: [],
            userAddress: {},
            userAddresses: [],
            addressLoading: false,
            logged: false,
            GPSAddress: {},
            numberHouse: '',
            selectedLocation: {},
            showDialog: false,
        };

        this.addressLoader = React.createRef();
        this.snacks = {}
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyLogin();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    async verifyLogin() {
        console.log('\n USER ADDRESSES ');

        let logged = false;
        let userAddresses = [];

        let token = await AsyncStorage.getItem('user_token');
    
        console.log('\nTOKEN: ', token);

        if(token !== 'FALSE') {
            logged = true;
        
            // Pegando objeto do usuário
            let user = JSON.parse(await AsyncStorage.getItem('user'));

            userAddresses = user.addresses;
        }
    
        // Pegando localização
        let GPSAddress = JSON.parse(await AsyncStorage.getItem('GPSAddress'));
        let currentAddress = await AsyncStorage.getItem('address');
        if(currentAddress !== 'NONE')
            currentAddress = JSON.parse(currentAddress)

        this.setState({ logged, GPSAddress, userAddress: currentAddress, userAddresses });
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header
                    goBack={async () => {

                        let screen = await AsyncStorage.getItem('screen');
                        await AsyncStorage.setItem('screen', 'NONE');
                        if(screen == 'HELP_REQUEST') {
                            await AsyncStorage.setItem('screen', 'ADDRESS');
                            this.props.navigation.pop();
                            this.props.navigation.pop();
                            this.props.navigation.push('MoreInfoHelpRequest');
                        }else if(screen == 'HELP_OFFER') {
                            await AsyncStorage.setItem('screen', 'ADDRESS');
                            this.props.navigation.pop();
                            this.props.navigation.pop();
                            this.props.navigation.push('MoreInfoHelpOffer');
                        }else if(screen == 'APPLY_OFFER') {
                            await AsyncStorage.setItem('screen', 'ADDRESS');
                            this.props.navigation.pop();
                            this.props.navigation.pop();
                            this.props.navigation.push('MoreInfoApplyOffer');
                        }else if(screen == 'HELP_REQ_APPLY') {
                            await AsyncStorage.setItem('screen', 'ADDRESS');
                            this.props.navigation.pop();
                            this.props.navigation.pop();
                            this.props.navigation.push('MoreInfoHelpRequestView');
                        }else if(screen == 'OFFER_LIST') {
                            this.props.navigation.pop();
                            this.props.navigation.push('OffersList')
                        }else if(screen == 'ASK_LIST') {
                            this.props.navigation.pop();
                            this.props.navigation.push('AskersList')
                        }
                    }} 
                    title="Seus Enderecos"  
                />                      

                <View style={styles.root}>
                    <View style={styles.cardContent}>
                        
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 6 }}>
                                <Input
                                    dense={true}
                                    mode='outlined'
                                    placeholder='Endereco'
                                    value={this.state.searchAddress}
                                    onChangeText={searchAddress => this.setState({ searchAddress })}
                                />  
                                </View>
                                <View style={{ flex: 1 }}>
                                <IconButton 
                                    icon='magnify'
                                    onPress={async () => {

                                        await this.setState({ addressLoading: true });
                                        this.addressLoader.show();
                                        
                                        let res = await Axios.getAddressByText(this.state.searchAddress, (error) => {
                                            this.addressLoader.close();
                                            this.setState({ addressLoader: false });
                                            console.log(error);
                                        });

                                        if(res) {
                                            if(res.data.data) {
                                                let addresses = [];
                                                for(let i = 0; i < res.data.data.length; i++) {
                                                    if(res.data.data[i].street)
                                                        addresses.push(res.data.data[i])
                                                }

                                                this.addressLoader.close();
                                                this.setState({ searchAddresses: addresses, addressLoading: false })
                                            }
                                        }else{
                                            this.addressLoader.close();
                                            this.setState({ searchAddresses: [], addressLoading: false });
                                        }
                                    }} 
                                />
                                </View>
                            </View>

                            <View style={{ flex: 2 }}>
                                { this.state.addressLoading
                                    ? <View style={{ flex: 1, alignContent:'center', alignItems: 'center', justifyContent: 'center' }}> 
                                        <Loader ref={ref=>this.addressLoader=ref} />
                                    </View>
                                    : <FlatList 
                                        ListEmptyComponent={
                                            <View style={{ alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                                <IconButton icon="map-marker" size={50} color="rgba(0,0,0,0.2)" />
                                                <Subheading>Nenhum endereço encontrado na busca</Subheading>
                                            </View>
                                        }
                                        showsVerticalScrollIndicator={false}
                                        data={this.state.searchAddresses}
                                        keyExtractor={item => item.label + item.street_number + item.locality + item.region + item.country}
                                        renderItem={({ item }) => {
                                            if(item.street) {
                                                return(
                                                    <View>
                                                        <TouchableOpacity 
                                                            style={styles.itemContent}
                                                            onPress={() => {
                                                                let selectedLocation = {
                                                                    lat: item.latitude,
                                                                    long: item.longitude,
                                                                    street: item.street,
                                                                    locality: item.locality
                                                                }

                                                                this.setState({ selectedLocation, showDialog: true })
                                                            }}
                                                        >
                                                            <Subheading>
                                                                {item.street}{item.street_number ? ',' + item.street_number : null}
                                                            </Subheading>
                                                            <Caption>
                                                                {item.locality ? item.locality : null}{item.region ? ' - ' + item.region : null}{item.country ? ' - ' + item.country : null}
                                                            </Caption>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            }
                                        }}
                                    />
                                }

                                <Divider />
                            </View>
                        </View>

                        <View style={{ flex: 2 }}>
                            
                            <View style={{ flex: 1, marginTop: 10 }}>
                                <Subheading style={styles.importantText}>Use sua localização</Subheading>
                                <TouchableOpacity style={this.state.userAddress === 'NONE' ? styles.inUseCardItem : styles.cardItem}
                                    onPress={async () => {
                                        await AsyncStorage.setItem('address', 'NONE');
                                        this.setState({ userAddress: 'NONE' })
                                    }}
                                >
                                    <View style={{ flexDirection: 'row' }}>
                                        <IconButton style={{ margin: 0, alignSelf: 'center' }} icon="crosshairs-gps" />
                                        <View style={{ paddingHorizontal: 10 }}>
                                            <Subheading>
                                                {this.state.GPSAddress.street}{this.state.GPSAddress.number !== null ? ', ' + this.state.GPSAddress.number : null}
                                            </Subheading>
                                            <Caption>
                                                {this.state.GPSAddress.city !== 'NULL' ? this.state.GPSAddress.city : null} 
                                            </Caption>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <Subheading style={styles.importantText}>Enderecos Salvos</Subheading>
                            { !this.state.logged
                                ? <View style={{ flex: 2, alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                    <Subheading>Faca login para ver seus enderecos salvos.</Subheading>
                                    <Button 
                                        mode='contained' 
                                        style={{ marginVertical: 10 }}
                                        onPress={ async () => {
                                            await AsyncStorage.setItem('sub_screen', 'ADDRESS');
                                            this.props.navigation.navigate('Login')
                                        }}
                                    >
                                        LOGIN
                                    </Button>
                                </View>

                                : <View style={{ flex: 2, marginTop: 10 }}>
                                    <FlatList
                                        ListEmptyComponent={
                                            <View style={{ alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                                <IconButton icon="map-marker" size={70} color="rgba(0,0,0,0.2)" />
                                                <Subheading>Você não possui endereços salvos.</Subheading>
                                            </View>
                                        }
                                        showsVerticalScrollIndicator={false}
                                        data={this.state.userAddresses}
                                        keyExtractor={item => item.id.toString()}
                                        renderItem={({ item }) => {
                                            if(item.street) {
                                                return(
                                                    <TouchableOpacity style={this.state.userAddress.street == item.street && this.state.userAddress.number == item.number ? styles.inUseCardItem : styles.cardItem}
                                                        onPress={async () => {
                                                            await AsyncStorage.setItem('address', JSON.stringify(item))
                                                            this.setState({ userAddress: item })
                                                        }}
                                                    >
                                                        <Subheading>
                                                            {item.street}, {item.number}
                                                        </Subheading>
                                                        <Caption>
                                                            {item.city}
                                                        </Caption>
                                                    </TouchableOpacity>
                                                )
                                            }
                                        }}
                                    />
                                </View>
                            }
                            
                        </View>
                    </View>
                </View>

                <Portal>
                    <Dialog visible={this.state.showDialog} onDismiss={() => this.setState({ showDialog: false })}>
                        <Dialog.Title>{this.state.logged ? 'Salve seu endereço' : 'Altere o endereço'}</Dialog.Title>
                        <Dialog.Content>
                            <Caption>{this.state.selectedLocation.street} - {this.state.selectedLocation.locality}</Caption>
                            <Input
                                dense={true}
                                mode='outlined'
                                placeholder='Número da casa'
                                value={this.state.numberHouse}
                                onChangeText={numberHouse => this.setState({ numberHouse })}
                            /> 
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button onPress={() => this.setState({ showDialog: false })}>Cancelar</Button>
                            <Button
                                onPress={async () => {

                                    let data = {
                                        lat: this.state.selectedLocation.lat,
                                        long: this.state.selectedLocation.long,
                                        street:  this.state.selectedLocation.street,
                                        number: this.state.numberHouse,
                                        city: this.state.selectedLocation.locality
                                    }

                                    if(this.state.logged) {
                                        let token = await AsyncStorage.getItem('user_token');

                                        let res = await Axios.post('address', data, token, async (error) => {
                                            console.log(error)
                                            await this.setState({ errorStatus: 'Houve um problema no servidor.'});
                                            this.snacks['errors'].show();
                                        });

                                        await AsyncStorage.setItem('address', JSON.stringify(res.data));
                            
                                        let user = await Axios.get('me', token)
                                            
                                        await AsyncStorage.setItem('user', JSON.stringify(user.data));

                                        this.setState({ userAddress: data, userAddresses: user.data.addresses, showDialog: false, searchAddress: '', searchAddresses: [] })
                                    }else{
                                        this.snacks['success'].show();
                                        await AsyncStorage.setItem('address', JSON.stringify(data));
                                        this.setState({userAddress: data , showDialog: false, searchAddresses: [], searchAddress: ''})
                                    }
                                }}
                            >
                                {this.state.logged ? 'Salvar' : 'Alterar'}
                            </Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>

                <CustomSnackbar
                    onRef={(ref)=> { this.snacks['success'] = ref }}
                    text={"Seu endereço foi alterado!"}
                />

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        borderRadius: 5,
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.6)',
        padding: 10,
        marginVertical: 5
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },

    inUseCardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgb(45, 117, 228)', padding: 10, marginVertical: 5 },
    cardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgba(0,0,0,0.6)', padding: 10, marginVertical: 5 }
})