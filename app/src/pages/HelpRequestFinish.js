import React, { Component } from 'react';
import { Subheading, Button, Caption, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import CustomSnackbar from '../components/CustomSnackbar';
import Loader from '../components/Loader';
import Axios from '../commom/Axios';

export default class HelpRequestFinish extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loadingButton: false,
            userOffer: {},
            loading: true
        };

        this.loader = React.createRef();
        this.snacks = {};
    }

    async componentDidMount() {

        this.loader.show();

        let userOffer = {};
        let notes = ""

        if(this.props.route.params.help.offers.length > 0) {
            let resUser = await Axios.get('user/' + this.props.route.params.help.offers[0].offering_user_id.toString());
            userOffer = resUser.data;
            notes = this.props.route.params.help.offers[0].notes;
        }

        this.loader.close();
        this.setState({ userOffer, notes, loading: false });

    }

    render() {

        if(this.state.loading) {
            return(
                <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}> 
                    <Loader ref={ref=>this.loader=ref}/>
                </View>
            )
        }

        return (
            <View style={{ flex: 1 }}>                      
                <View style={[styles.root, { marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }]}>    
                    
                   <View style={{ flex: 1}}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, marginVertical: 10, borderWidth: 1, padding: 10, borderRadius: 10, borderColor: 'rgba(64, 68, 76, 0.3)' }}>
                                <Subheading style={[styles.importantText, { textAlign: 'center'}]}>Resumo do pedido para {this.props.route.params.help.type}</Subheading>
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    data={this.props.route.params.help.items}
                                    keyExtractor={item => item.name.toString()}
                                    renderItem={({ item, index }) => {
                                        return(
                                            <View style={{ flexDirection: 'row' }}>
                                                <Subheading style={styles.importantText}>{index+1}. </Subheading>
                                                <Subheading>
                                                    {item.name} ({item.quantity}) até R$ {item.max_cost}
                                                </Subheading>
                                            </View>
                                        )
                                    }}
                                />
                                <Subheading style={styles.importantText}>Valor máximo: R$ {this.props.route.params.help.max_cost}</Subheading>
                            </View>
                        </View>

                        <View style={{ flex: 2 }}>
                           <View style={{ flex: 1 }}>
                                <Subheading style={styles.importantText}>Endereço para entrega</Subheading>
                                <View style={styles.cardItem}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <IconButton style={{ margin: 0, alignSelf: 'center' }} icon="crosshairs-gps" />
                                        <View style={{ paddingHorizontal: 10 }}>
                                            <Subheading>
                                                {this.props.route.params.help.address.street}{this.props.route.params.help.address.number !== null ? ', ' + this.props.route.params.help.address.number : null}
                                            </Subheading>
                                            <Caption>
                                                {this.props.route.params.help.address.city !== 'NULL' ? this.props.route.params.help.address.city : null} 
                                            </Caption>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={{ flex: 1 }}>
                                <Subheading style={styles.importantText}>Data e Hora para entrega</Subheading>
                                <View style={{ flexDirection: 'row', marginVertical: 5, alignContent: 'center', alignItems: 'center' }}>
                                    <Subheading>Dia {moment(new Date(this.props.route.params.help.help_day)).format('DD/MM')} das {moment(new Date(this.props.route.params.help.min_available_hour)).format('HH:mm')}h até as {moment(new Date(this.props.route.params.help.max_available_hour)).format('HH:mm')}h</Subheading>
                                </View>
                            </View> 

                            <View style={{ flex: 1 }}>
                                <Subheading style={styles.importantText}>Informações de contato</Subheading>
                                <Subheading>{this.state.userOffer.name}</Subheading>
                                <Subheading>{this.state.userOffer.phone}</Subheading>
                            </View>
                            
                            { this.state.notes.length > 0
                                ? <View style={{ flex: 1 }}>
                                    <Subheading style={styles.importantText}>Observações do voluntário</Subheading>
                                    <Subheading>{this.state.notes}</Subheading>
                                </View>
                                : null
                            }

                            <View style={{ flex: 1, marginTop: 20 }}>

                                <Button mode="contained" loading={this.state.loadingButton}
                                    onPress={ async () => {

                                        await this.setState({ loadingButton: true });

                                        let token = await AsyncStorage.getItem('user_token');
                                        let res = await Axios.put('help/' + this.props.route.params.help.id.toString() + '/fulfill', {}, token);
                                        
                                        if(res.status == 200)
                                            this.snacks['success'].show();

                                    }}
                                >
                                    Finalizar Pedido de ajuda
                                </Button>

                                <Button 
                                    mode="contained" 
                                    style={{ marginTop: 10 }} 
                                    onPress={() => this.props.navigation.goBack()}
                                > 
                                    Voltar
                                </Button>
                            </View>
                        </View>

                        <CustomSnackbar
                            onRef={(ref) => { this.snacks['success'] = ref}}
                            text={"Pedido de ajuda finalizado com sucesso!"}
                            onClose={() => {
                                this.props.navigation.pop();
                                this.props.navigation.push('Historic');
                            } }
                        />
                    </View>
                    
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        borderRadius: 5,
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.6)',
        padding: 10,
        marginVertical: 5
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },

    inUseCardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgb(45, 117, 228)', padding: 10, marginVertical: 5 },
    cardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgba(0,0,0,0.6)', padding: 10, marginVertical: 5 }
})