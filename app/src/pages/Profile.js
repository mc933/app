import React, { Component } from 'react';
import { Button, Title, Avatar, Subheading } from 'react-native-paper';
import { StyleSheet, View, KeyboardAvoidingView, Platform, StatusBar, Image, Dimensions } from 'react-native';
import CustomSnackbar from '../components/CustomSnackbar';
import Input from '../components/Input';
import AsyncStorage from '@react-native-community/async-storage';
import * as EmailValidator from 'email-validator';
import { validate } from 'gerador-validador-cpf'
import Axios from '../commom/Axios';

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            name: '',
            email: '',
            ddd: '',
            phone: '',
            cpf: '',
            pass: '',
            confpass: '',
            logged: false,
            loadingButton: false,
        };

        this.snacks = {}

    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyLogin();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    async verifyLogin() {

        let logged = false;
        let token = await AsyncStorage.getItem('user_token');

        if(token !== 'FALSE') {
            logged = true;

            // Pegando objeto do usuário
            let user = JSON.parse(await AsyncStorage.getItem('user'));

            this.setState({
                id: user.id,
                name: user.name,
                email: user.email,
                ddd: user.phone.substring(0,2),
                phone: user.phone.substring(2, user.phone.length),
                cpf: user.cpf,
                logged
            });
        }else
            this.setState({ logged });
    }

    async handleSave() {

        await this.setState({ loadingButton: true });

        if(this.state.name == '') {
            await this.setState({ errorStatus: 'Campo nome requirido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(this.state.email == '') {
            await this.setState({ errorStatus: 'Campo email requirido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(EmailValidator.validate(this.state.email) == false) {
            await this.setState({ errorStatus: 'Formato de email inválido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(this.state.cpf.length < 11) {
            await this.setState({ errorStatus: 'CPF inválido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(validate(this.state.cpf) == false) {
            await this.setState({ errorStatus: 'CPF inválido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(this.state.ddd == '' || this.state.phone == '') {
            await this.setState({ errorStatus: 'Campo ddd+telefone requirido.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        if(this.state.pass != '' && this.state.pass != this.state.confpass) {
            await this.setState({ errorStatus: 'Senhas não correspondentes.', loadingButton: false })
            this.snacks['errors'].show();
            return;
        }

        let data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.ddd + this.state.phone,
            password: this.state.pass,
            cpf: this.state.cpf
        };

        // Verificando se a senha foi alterada ou não
        if(data.password == '')
            data.password = await AsyncStorage.getItem('pass');

        let token = await AsyncStorage.getItem('user_token');

        let res = await Axios.put('user/' + this.state.id, data, token);

        if(res.status == 200) {
            await AsyncStorage.setItem('user', JSON.stringify(res.data));
            await this.setState({ pass: '', confpass: '', loadingButton: false })
            this.snacks['success'].show();
        }else
            this.this.setState({ loadingButton: false })
    }
    render() {
        return (
            <View style={{ flex: 1, paddingTop: Platform.SO == 'ios' ? 44 : StatusBar.currentHeight }}>
                { this.state.logged
                    ? <KeyboardAvoidingView
                        behavior={Platform.OS == 'ios' ? "padding" : 'height'}
                        style={styles.root}
                    >

                        <View style={styles.loginContent}>
                            <Title>Editar Perfil</Title>

                            <View style={{alignSelf: 'center', paddingVertical: 20 }}>
                                <Avatar.Text
                                    style={styles.avatar}
                                    size={120}
                                    label={this.state.name.match(/\b(\w)/g).join('')}
                                />
                            </View>

                            <View style={styles.card}>
                                <Input
                                    //icon='account'
                                    dense={true}
                                    mode='outlined'
                                    style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                    placeholder='Nome'
                                    value={this.state.name}
                                    onChangeText={name => this.setState({ name })}
                                />
                                <Input
                                    //icon='at'
                                    dense
                                    style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                    mode='outlined'
                                    placeholder='Email'
                                    value={this.state.email}
                                    onChangeText={email => this.setState({ email })}
                                />

                                <Input
                                    //icon='identifier'
                                    dense
                                    style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                    mode='outlined'
                                    placeholder='CPF'
                                    value={this.state.cpf}
                                    onChangeText={cpf => this.setState({ cpf })}
                                />

                                <View style={{flexDirection: 'row' }}>
                                    <View style={{ flex: 1, marginRight: 5 }}>
                                        <Input
                                            //icon="phone"
                                            dense
                                            style={{ backgroundColor: 'white', marginLeft: 10 }}
                                            mode='outlined'
                                            placeholder='11'
                                            type='number'
                                            value={this.state.ddd}
                                            onChangeText={ddd => this.setState({ ddd })}
                                        />
                                    </View>
                                    <View style={{ flex: 2 }}>
                                        <Input
                                            dense
                                            style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                            mode='outlined'
                                            type='number'
                                            placeholder='999999999'
                                            value={this.state.phone}
                                            onChangeText={phone => this.setState({ phone })}
                                        />
                                    </View>
                                </View>
                            </View>

                            <View style={styles.card}>
                                <Title>Alterar Senha</Title>
                                <Input
                                    //icon='key'
                                    dense
                                    secureTextEntry={true}
                                    style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                    mode='outlined'
                                    placeholder='Senha'
                                    value={this.state.pass}
                                    onChangeText={pass => this.setState({ pass })}
                                />
                                <Input
                                    //icon='key'
                                    dense
                                    secureTextEntry={true}
                                    style={{ backgroundColor: 'white', marginHorizontal: 10 }}
                                    mode='outlined'
                                    placeholder='Confirmar Senha'
                                    value={this.state.confpass}
                                    onChangeText={confpass => this.setState({ confpass })}
                                />
                            </View>

                            <View style={styles.buttonContent}>
                                <Button
                                    loading={this.state.loadingButton}
                                    onPress={() => this.handleSave()}
                                    contentStyle={styles.button}
                                    mode="contained">Salvar</Button>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                    : <View style={{ flex: 1, alignContent:'center', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                        <Subheading>Lembrete: Fique em casa</Subheading>
                        <Image
                            style={{ width: Dimensions.get('window').width*0.6, height: Dimensions.get('window').height*0.3 }}
                            source={require('../assets/images/login_profile.jpg')}
                        />
                        <Subheading>Faça login para ver seu perfil</Subheading>
                        <Button
                            mode='contained'
                            style={{ marginVertical: 10 }}
                            onPress={ async () => {
                                await AsyncStorage.setItem('screen', 'PROFILE');
                                this.props.navigation.navigate('Login');
                            }}
                        >
                            LOGIN
                        </Button>
                    </View>
                }

                <CustomSnackbar
                    onRef={(ref)=> { this.snacks['success'] = ref }}
                    text="Sucesso! Seu perfil foi salvo"
                />

                <CustomSnackbar
                    error={true}
                    onRef={(ref)=> { this.snacks['errors'] = ref }}
                    text={this.state.errorStatus}
                />

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 20,
        flex: 1
    },
    loginContent: {
        marginTop: 15,
        flex: 1
    },
    buttonContent: {
        paddingVertical: 10,
        flex: 1
    },
    button: {
        paddingVertical: 10,
    },
    card: {
        paddingVertical: 10
    },
    avatar: {
        borderWidth: 2, borderColor: 'rgba(64, 68, 76, 1)',
    },

})
