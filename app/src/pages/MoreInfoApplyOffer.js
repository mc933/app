import React, { Component } from 'react';
import { Headline, Subheading, Button, Caption, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import DateTimePicker from '../components/DateTimePicker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import Input from '../components/Input';
import CustomSnackbar from '../components/CustomSnackbar';
import Axios from '../commom/Axios';

export default class MoreInfoApplyOffer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            address: {},
            logged: false,
            address_id: 0,
            help_day: new Date(),
            min_available_hour: new Date(),
            max_available_hour: new moment().add('1', 'h').toDate(),
            notes: '',
            type: '',
            items: [],
            max_cost: 0,
            help: {},
            loadingButton: false
        };

        this.addressLoader = React.createRef();
        this.snacks = {};
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyRoute();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    async verifyRoute() {

        console.log('\n MORE INFO ');

        // Verificando se está logado
        let logged = false;
        let token = await AsyncStorage.getItem('user_token');
        console.log('\nTOKEN: ', token);
        if(token !== 'FALSE')
            logged = true;
    
        // Pegando localização
        let address = JSON.parse(await AsyncStorage.getItem('GPSAddress'));
        let currentAddress = await AsyncStorage.getItem('address');
        if(currentAddress !== 'NONE')
            address = JSON.parse(currentAddress)

        // Verificando de que rota veio para pegar os parametros
        let helpRequest = {};
        let helpOffer = {};
        let screen = await AsyncStorage.getItem('screen');
        if(screen == 'LOGIN' || screen == 'ADDRESS') {
            helpRequest = JSON.parse(await AsyncStorage.getItem('help_req'));
            helpOffer = JSON.parse(await AsyncStorage.getItem('help_offer'));
        }else{
            helpRequest = {
                address_id: address.id,
                help_day: new Date(),
                min_available_hour: new Date(),
                max_available_hour: new moment().add('1', 'h').toDate(),
                notes: '',
                type: this.props.route.params.help.type,
                items: this.props.route.params.items,
                max_cost: this.props.route.params.max_cost
            };

            helpOffer = this.props.route.params.help;
        }

        this.setState({ 
            logged, 
            address,
            address_id: helpRequest.address_id,
            help_day: helpRequest.help_day,
            min_available_hour: helpRequest.min_available_hour,
            max_available_hour: helpRequest.max_available_hour,
            notes: helpRequest.notes,
            type: helpRequest.type,
            items: helpRequest.items,
            max_cost: helpRequest.max_cost,
            help: helpOffer
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>                      
                <View style={[styles.root, { marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }]}>
                    
                    <View style={{ flex: 1 }}>

                        <View style={{ flex: 3, marginVertical: 10, borderWidth: 1, padding: 10, borderRadius: 10, borderColor: 'rgba(64, 68, 76, 0.3)' }}>
                            <Subheading style={[styles.importantText, { textAlign: 'center'}]}>Resumo do pedido para {this.state.type}</Subheading>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={this.state.items}
                                keyExtractor={item => item.name.toString()}
                                renderItem={({ item, index }) => {
                                    return(
                                        <View style={{ flexDirection: 'row' }}>
                                            <Subheading style={styles.importantText}>{index+1}. </Subheading>
                                            <Subheading>
                                                {item.name} ({item.quantity}) até R$ {item.max_cost}
                                            </Subheading>
                                        </View>
                                    )
                                }}
                            />
                        </View>

                        <View style={{ flex: 1}}>
                            <Input 
                                mode='outlined'
                                placeholder='Quer fazer alguma observação?'
                                value={this.state.notes}
                                onChangeText={notes => this.setState({ notes })}
                            />
                        </View>
                    </View>

                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <Subheading style={styles.importantText}>Endereço para entrega</Subheading>
                            <TouchableOpacity style={styles.cardItem}
                                onPress={async () => {

                                    let helpRequest = {
                                        max_cost: this.state.max_cost,
                                        notes: this.state.notes,
                                        lat: this.state.address.lat,
                                        long: this.state.address.long,
                                        help_day: this.state.help_day,
                                        min_available_hour: this.state.min_available_hour,
                                        max_available_hour: this.state.max_available_hour,
                                        type: this.state.type,
                                        items: this.state.items
                                    };

                                    await AsyncStorage.setItem('help_req', JSON.stringify(helpRequest));
                                    await AsyncStorage.setItem('help_offer', JSON.stringify(this.state.help));

                                    await AsyncStorage.setItem('screen', 'APPLY_OFFER');
                                    this.props.navigation.navigate('UserAddresses')
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    <IconButton style={{ margin: 0, alignSelf: 'center' }} icon="crosshairs-gps" />
                                    <View style={{ paddingHorizontal: 10 }}>
                                        <Subheading>
                                            {this.state.address.street}{this.state.address.number !== null ? ', ' + this.state.address.number : null}
                                        </Subheading>
                                        <Caption>
                                            {this.state.address.city !== 'NULL' ? this.state.address.city : null} 
                                        </Caption>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Subheading style={styles.importantText}>Data e Hora para entrega</Subheading>
                            <View style={{ flexDirection: 'row', marginVertical: 5, alignContent: 'center', alignItems: 'center' }}>
                                <Subheading>Data para entrega: </Subheading>
                                <DateTimePicker 
                                    mode={'date'}
                                    value={this.state.help_day}
                                    onChange={(event, selectedDate) => this.setState({ help_day: selectedDate })}
                                />
                            </View>
                            <View style={{ marginVertical: 5, flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
                                <Subheading>Estarei em casa das </Subheading>
                                <DateTimePicker 
                                    mode={'time'}
                                    value={this.state.min_available_hour}
                                    onChange={(event, selectedDate) => this.setState({ min_available_hour: selectedDate })}
                                />
                                <Subheading> às </Subheading>
                                <DateTimePicker 
                                    mode={'time'}
                                    value={this.state.max_available_hour}
                                    onChange={(event, selectedDate) => this.setState({ max_available_hour: selectedDate })}
                                />
                            </View>
                        </View>

                        <View style={{ flex: 1, marginTop: 20 }}>

                            {this.state.logged
                                ? <Button mode="contained" loading={this.state.loadingButton}
                                    onPress={ async () => {
                                        
                                        await this.setState({ loadingButton: true });

                                        // Pegando token
                                        let token = await AsyncStorage.getItem('user_token');

                                        let data = { 
                                            address_id: this.state.address_id,
                                            max_cost: this.state.max_cost,
                                            notes: this.state.notes,
                                            help_day: this.state.help_day.toString(),
                                            min_available_hour: this.state.min_available_hour.toString(),
                                            max_available_hour: this.state.max_available_hour.toString(),
                                            type: this.state.type,
                                            items: this.state.items
                                        };

                                        // Verificando endereço
                                        if(data.address_id == undefined) {
                                            let add = Object.assign({}, this.state.address);

                                            if(add.city == undefined)
                                                add.city = 'Sem info';

                                            let addressReq = await Axios.post('address', add, token);

                                            if(addressReq.status == 200) {
                                                await AsyncStorage.setItem('address', JSON.stringify(addressReq.data));
                                                await this.setState({ address: addressReq.data });
                                                data.address_id = addressReq.data.id
                                            };
                                        }

                                        let res = await Axios.put('help_offer/' + this.state.help.id.toString() + '/ask', data, token);

                                        if(res.status == 200)
                                            this.snacks['success'].show();

                                    }}
                                >
                                    Finalizar
                                </Button>
                                : <Button mode="contained"
                                    onPress={async () => {

                                        let helpRequest = {
                                            max_cost: this.state.max_cost,
                                            notes: this.state.notes,
                                            address_id: this.state.address.id,
                                            help_day: this.state.help_day,
                                            min_available_hour: this.state.min_available_hour,
                                            max_available_hour: this.state.max_available_hour,
                                            type: this.state.type,
                                            items: this.state.items
                                        };

                                        await AsyncStorage.setItem('help_req', JSON.stringify(helpRequest));
                                        await AsyncStorage.setItem('help_offer', JSON.stringify(this.state.help))

                                        await AsyncStorage.setItem('screen', 'APPLY_OFFER');
                                        this.props.navigation.navigate('Login')
                                    }}
                                >
                                    Faça login para finalizar
                                </Button>
                            }

                            <Button 
                                mode="contained" 
                                style={{ marginTop: 10 }} 
                                onPress={() => this.props.navigation.goBack()}
                            > 
                                Voltar
                            </Button>
                        </View>
                    </View>
                </View>

                <CustomSnackbar
                    onRef={(ref) => { this.snacks['success'] = ref}}
                    text={"Pedido de ajuda solicitado, o voluntário irá te ajudar!"}
                    onClose={async () =>{
                        await AsyncStorage.setItem('screen', 'NONE');
                        await AsyncStorage.setItem('help_req', 'NONE');
                        await AsyncStorage.setItem('help_offer', 'NONE');
                        this.props.navigation.pop();
                        this.props.navigation.pop();
                        this.props.navigation.push("OffersList");
                    } }
                />

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        borderRadius: 5,
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.6)',
        padding: 10,
        marginVertical: 5
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },

    inUseCardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgb(45, 117, 228)', padding: 10, marginVertical: 5 },
    cardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgba(0,0,0,0.6)', padding: 10, marginVertical: 5 }
})