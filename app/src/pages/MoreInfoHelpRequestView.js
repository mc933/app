import React, { Component } from 'react';
import { Subheading, Button, Caption, IconButton } from 'react-native-paper';
import { StyleSheet, View, FlatList, Platform, StatusBar } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import CustomSnackbar from '../components/CustomSnackbar';
import Loader from '../components/Loader';
import Axios from '../commom/Axios';

export default class MoreInfoHelpRequestView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            logged: false,
            help: {},
            loading: true,
            loadingButton: false
        };

        this.loader = React.createRef();
        this.snacks = {};
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.verifyRoute();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    async verifyRoute() {
        console.log('\n MORE INFO VIEW');

        await this.setState({ loading: true })
        this.loader.show();

        // Verificando se está logado
        let logged = false;
        let token = await AsyncStorage.getItem('user_token');
        console.log('\nTOKEN: ', token);
        if(token !== 'FALSE')
            logged = true;

        // Pegando parâmetros
        let help = {};
        let screen = await AsyncStorage.getItem('screen');
        if(screen == 'LOGIN')
            help = JSON.parse(await AsyncStorage.getItem('help_req_apply'))
        else
            help = this.props.route.params.help;

        this.loader.close();
        this.setState({ 
            logged, 
            help,
            loading: false
        });
    }

    render() {

        return (
            <View style={{ flex: 1 }}>                      
                <View style={[styles.root, { marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }]}>    
                    
                    { this.state.loading
                        ? <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}> 
                            <Loader ref={ref=>this.loader=ref}/>
                        </View>
                        : !this.state.logged
                            ? <View style={{ flex: 1, alignContent:'center', alignItems: 'center', justifyContent: 'center' }}>
                                <Subheading>Faca login para ver o pedido de ajuda</Subheading>
                                <Button
                                    mode='contained'
                                    style={{ marginVertical: 10 }}
                                    onPress={ async () => {
                                        await AsyncStorage.setItem('help_req_apply', JSON.stringify(this.state.help));
                                        await AsyncStorage.setItem('screen', 'HELP_REQ_APPLY');
                                        this.props.navigation.navigate('Login')
                                    }}
                                >
                                    LOGIN
                                </Button>
                            </View>
                            : <View style={{ flex: 1}}>
                                <View style={{ flex: 1 }}>
                                    <View style={{ flex: 1, marginVertical: 10, borderWidth: 1, padding: 10, borderRadius: 10, borderColor: 'rgba(64, 68, 76, 0.3)' }}>
                                        <Subheading style={[styles.importantText, { textAlign: 'center'}]}>Resumo do pedido para {this.state.help.type}</Subheading>
                                        <FlatList
                                            showsVerticalScrollIndicator={false}
                                            data={this.state.help.items}
                                            keyExtractor={item => item.name.toString()}
                                            renderItem={({ item, index }) => {
                                                return(
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Subheading style={styles.importantText}>{index+1}. </Subheading>
                                                        <Subheading>
                                                            {item.name} ({item.quantity}) até R$ {item.max_cost}
                                                        </Subheading>
                                                    </View>
                                                )
                                            }}
                                        />
                                        <Subheading style={styles.importantText}>Valor máximo: R$ {this.state.help.max_cost}</Subheading>
                                    </View>
                                </View>

                                <View style={{ flex: 2 }}>
                                    <View style={{ flex: 1 }}>
                                        <Subheading style={styles.importantText}>Endereço para entrega</Subheading>
                                        <View style={styles.cardItem}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <IconButton style={{ margin: 0, alignSelf: 'center' }} icon="crosshairs-gps" />
                                                <View style={{ paddingHorizontal: 10 }}>
                                                    <Subheading>
                                                        {this.state.help.address.street}{this.state.help.address.number !== null ? ', ' + this.state.help.address.number : null}
                                                    </Subheading>
                                                    <Caption>
                                                        {this.state.help.address.city !== 'NULL' ? this.state.help.address.city : null} 
                                                    </Caption>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <Subheading style={styles.importantText}>Data e Hora para entrega</Subheading>
                                        <View style={{ flexDirection: 'row', marginVertical: 5, alignContent: 'center', alignItems: 'center' }}>
                                            <Subheading>Dia {moment(new Date(this.state.help.help_day)).format('DD/MM')} das {moment(new Date(this.state.help.min_available_hour)).format('HH:mm')}h até as {moment(new Date(this.state.help.max_available_hour)).format('HH:mm')}h</Subheading>
                                        </View>
                                    </View> 

                                    <View style={{ flex: 1 }}>
                                        <Subheading style={styles.importantText}>Informações de contato</Subheading>
                                        <Subheading>{this.state.help.user.name}</Subheading>
                                        <Subheading>{this.state.help.user.phone}</Subheading>
                                    </View> 
                                    
                                    <View style={{ flex: 1, marginTop: 20 }}>

                                        <Button mode="contained" loading={this.state.loadingButton}
                                            onPress={ async () => {

                                                await this.setState({ loadingButton: true });

                                                // Pegando endereço do usuário
                                                let locationResult = {};
                                                let address = await AsyncStorage.getItem('address');
 
                                                // Se tiver endereço na cache
                                                if(address !== 'NONE') {
                                                    locationResult = JSON.parse(address);
                                                }else{
                                                    let gpsAddress = await AsyncStorage.getItem('GPSAddress');
                                                    if(gpsAddress !== 'NONE')
                                                        locationResult = JSON.parse(gpsAddress);
                                                }

                                                // Criando uma help_offer
                                                let helpOffer = {
                                                    lat: locationResult.lat,
                                                    long: locationResult.long,
                                                    max_cost: this.state.help.max_cost,
                                                    notes: "",
                                                    help_day: this.state.help.help_day,
                                                    hour: new Date().toString(),
                                                    type: this.state.help.type
                                                }

                                                let token = await AsyncStorage.getItem('user_token');
                                                let res = await Axios.put('help/' + this.state.help.id + '/offer', helpOffer, token);
                                                
                                                if(res.status == 200)
                                                    this.snacks['success'].show();

                                            }}
                                        >
                                            Ajudar!
                                        </Button>

                                        <Button 
                                            mode="contained" 
                                            style={{ marginTop: 10 }} 
                                            onPress={() => this.props.navigation.goBack()}
                                        > 
                                            Voltar
                                        </Button>
                                    </View>
                                </View>

                                <CustomSnackbar
                                    onRef={(ref) => { this.snacks['success'] = ref}}
                                    text={"Ajuda Oferecida, " + this.state.help.user.name + ' está aguardando sua chegada.'}
                                    onClose={async () =>{
                                        await AsyncStorage.setItem('screen', 'NONE');
                                        await AsyncStorage.setItem('help_req_apply', 'NONE');
                                        this.props.navigation.pop();
                                        this.props.navigation.push("AskersList");
                                    } }
                                />
                            </View>
                    }
                    
                </View>

                

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        flex: 1,
        paddingTop: 10
    },
    itemContent: {
        borderRadius: 5,
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.6)',
        padding: 10,
        marginVertical: 5
    },
    importantText: {
        fontWeight: 'bold',
        textAlign: 'justify'
    },

    inUseCardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgb(45, 117, 228)', padding: 10, marginVertical: 5 },
    cardItem: { borderRadius: 5, borderWidth: 1, borderColor: 'rgba(0,0,0,0.6)', padding: 10, marginVertical: 5 }
})