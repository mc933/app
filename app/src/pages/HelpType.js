import React, { Component } from 'react';
import { Card, Button, IconButton, Caption, Title } from 'react-native-paper';
import { StyleSheet, View, Platform, StatusBar } from 'react-native';
import Header from '../components/Header';

export default class CodeVerify extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: '',
        };
    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: Platform.OS == 'ios' ? 44 : StatusBar.currentHeight }}>

                <View style={styles.root}>
                    
                    <View style={styles.loginContent}>

                        <Title style={styles.title}>Onde você gostaria de Comprar?</Title>
                        <View style={{ flexDirection: 'row'}}>
                            <View style={styles.buttonContainer}>
                                <IconButton 
                                    style={styles.button}
                                    icon="cart"
                                    size={64}
                                    color='rgba(64, 68, 76, 0.6)' 
                                    onPress={() => this.props.navigation.navigate('Todoist', { type: 'MERCADO' })}                   
                                />
                                <Caption>Mercado</Caption>
                            </View>
                            
                            <View style={styles.buttonContainer}>
                                <IconButton 
                                    style={styles.button}
                                    icon="baguette"
                                    size={64}
                                    color='rgba(64, 68, 76, 0.6)'
                                    onPress={() => this.props.navigation.navigate('Todoist', { type: 'PADARIA' })}
                                />
                                <Caption>Padaria</Caption>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row'}}>
                            <View style={styles.buttonContainer}>
                                <IconButton
                                    style={styles.button}
                                    icon="hospital-box"
                                    size={64}
                                    color='rgba(64, 68, 76, 0.6)'
                                    onPress={() => this.props.navigation.navigate('Todoist', { type: "FARMACIA" })}
                                />
                                <Caption>Farmácia</Caption>
                            </View>
                            <View style={styles.buttonContainer}>
                                <IconButton
                                    style={styles.button}
                                    icon="basket"
                                    size={64}
                                    color='rgba(64, 68, 76, 0.6)'
                                    onPress={() => this.props.navigation.navigate('Todoist', { type: 'FEIRA' })}
                                />
                                <Caption>Feira</Caption>
                            </View>
                        </View>
                    </View>

                    <Button mode="contained" style={{ marginVertical: 10 }} onPress={() => this.props.navigation.goBack()}> 
                        Cancelar
                    </Button>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingHorizontal: 20
    },
    cardContent: {
        paddingVertical: 25
    },
    loginContent: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        borderWidth: 1,
        borderColor: 'rgba(64, 68, 76, 0.6)',
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        margin: 20
    },

    title: {
        //color: '#000',
        //fontWeight: 'bold',
        //marginTop: 30,
        color: 'rgba(64, 68, 76, 1)',
        //fontWeight: 'bold',
        textAlign: 'center',
        paddingVertical: 20
    }
})