import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { FontAwesome5 } from '@expo/vector-icons';

//login & register
import Login from './pages/Login';
import NewUser from './pages/NewUser';
import OffersList from './pages/OffersList';
import AskersList from './pages/AskersList';
import Todoist from './pages/Todoist';
import HelpType from './pages/HelpType';
import OfferType from './pages/OfferType';
import WaitForHelp from './pages/WaitForHelp';
import WaitForAsker from './pages/WaitForAsker'
import UserAddresses from './pages/UserAddresses';
import Profile from './pages/Profile';
import Historic from './pages/Historic';
import MoreInfoHelpRequest from './pages/MoreInfoHelpRequest';
import MoreInfoHelpOffer from './pages/MoreInfoHelpOffer';
import MoreInfoHelpRequestView from './pages/MoreInfoHelpRequestView';
import TodoistApplyOffer from './pages/TodoistApplyOffer';
import MoreInfoApplyOffer from './pages/MoreInfoApplyOffer';
import HelpRequestFinish from './pages/HelpRequestFinish';
import HelpOfferFinish from './pages/HelpOfferFinish';

const AskHelpStack = createStackNavigator();
const HelpOfferStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const HistoricStack = createStackNavigator();
const LoginStack = createStackNavigator();

function LoginStackScreen() {
	return(
		<LoginStack.Navigator
			screenOptions={{ headerShown: false }}
		>
			<LoginStack.Screen name="Login" component={Login} />
			<LoginStack.Screen name="NewUser" component={NewUser} />
		</LoginStack.Navigator>
	)
}

function AskHelpStackScreen() {
	return (
		<AskHelpStack.Navigator
			screenOptions={{ headerShown: false }}
		>
			<AskHelpStack.Screen name="OffersList" component={OffersList} />
			<AskHelpStack.Screen name="UserAddresses" component={UserAddresses} />
			<AskHelpStack.Screen name="HelpType" component={HelpType} />
			<AskHelpStack.Screen name="Todoist" component={Todoist} />
			<AskHelpStack.Screen name="MoreInfoHelpRequest" component={MoreInfoHelpRequest} />
			<AskHelpStack.Screen name="TodoistApplyOffer" component={TodoistApplyOffer} />
			<AskHelpStack.Screen name="MoreInfoApplyOffer" component={MoreInfoApplyOffer} />
		</AskHelpStack.Navigator>
	)
}

function HelpOfferStackScreen() {
	return (
		<HelpOfferStack.Navigator
			screenOptions={{ headerShown: false }}
		>
			<HelpOfferStack.Screen name="AskersList" component={AskersList} />
			<HelpOfferStack.Screen name="OfferType" component={OfferType} />
			<HelpOfferStack.Screen name="MoreInfoHelpOffer" component={MoreInfoHelpOffer} />
			<HelpOfferStack.Screen name="UserAddresses" component={UserAddresses} />
			<HelpOfferStack.Screen name="MoreInfoHelpRequestView" component={MoreInfoHelpRequestView} />
		</HelpOfferStack.Navigator>
	)
}

function ProfileStackScreen() {
	return (
		<ProfileStack.Navigator
			screenOptions={{ headerShown: false, tabBarVisible: false }}
		>
			<ProfileStack.Screen name="Profile" component={Profile} />
		</ProfileStack.Navigator>
	)
}

function HistoricStackScreen() {
	return (
		<HistoricStack.Navigator
			screenOptions={{ headerShown: false }}
		>
			<HistoricStack.Screen name="Historic" component={Historic} />
			<HistoricStack.Screen name="HelpRequestFinish" component={HelpRequestFinish} />
			<HistoricStack.Screen name="HelpOfferFinish" component={HelpOfferFinish} />
		</HistoricStack.Navigator>
	)
}

const Tab = createBottomTabNavigator();

export default function Routes() {
	return (
		<NavigationContainer>
			<Tab.Navigator
				screenOptions={({ route }) => ({
					tabBarButton: [
						"Login"
					].includes(route.name)
						? () => {
							return null
						}
						: undefined,
				})}
			>
				<Tab.Screen
					name="OffersList"
					component={AskHelpStackScreen}
					options={{
						tabBarLabel: 'Pedir Ajuda',
						tabBarIcon: ({ color }) => (
							<MaterialCommunityIcons name="cart-outline" color={color} size={26} />
						),
					}}
				/>

				<Tab.Screen
					name="AskersList"
					component={HelpOfferStackScreen}
					options={{
						tabBarLabel: 'Oferecer Ajuda',
						tabBarIcon: ({ color }) => (
							<FontAwesome5 name="hands-helping" color={color} size={26} />
						),
					}}
				/>

				<Tab.Screen
					name="Historic"
					component={HistoricStackScreen}
					options={{
						tabBarLabel: 'Histórico',
						tabBarIcon: ({ color }) => (
							<MaterialCommunityIcons name="clock-outline" color={color} size={26} />
						),
					}}
				/>

				<Tab.Screen
					name="Profile"
					component={ProfileStackScreen}
					options={{
						tabBarLabel: 'Perfil',
						tabBarIcon: ({ color }) => (
							<MaterialCommunityIcons name="account-circle-outline" color={color} size={26} />
						),
					}}
				/>

				<Tab.Screen
					name="Login"
					component={LoginStackScreen}
					options={{ tabBarVisible: false }}
				/>

			</Tab.Navigator>
		</NavigationContainer>
	)
}