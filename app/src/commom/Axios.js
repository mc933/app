import axios from 'axios';
import qs from 'qs';
import jose from 'jwt-lite';
import AsyncStorage from '@react-native-community/async-storage';

const url_api = 'http://esther.felipevr.com/';

export default class Axios {

    static async verifyValidToken(token) {

        let valid_token = token;

        let res = await jose.decode(valid_token);

        let exp = parseInt(res.claimsSet.exp);
        let timestamp = Math.floor(new Date().getTime() / 1000);

        if(exp-timestamp <= 0) {

            let user = JSON.parse(await AsyncStorage.getItem('user'));
            let pass = await AsyncStorage.getItem('pass');

            valid_token = await this.token(user.email, pass);
            await AsyncStorage.setItem('user_token', valid_token);
        }

        return valid_token;
    };

    static async token(username, password, failureCallback = null) {
        let data = {
            username,
            password
        };

        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        let res = await axios.post(url_api + 'token', qs.stringify(data), config)
        .catch(async error => {
            if(failureCallback)
                failureCallback(error);
            else
                console.error(error);
        }); 

        return res.data.access_token;
    };

    static async get(path, token = null, failureCallback = null) {

        let res;

        if(token) {

            let valid_token = await this.verifyValidToken(token);

            let config = {
                headers: { Authorization: `Bearer ${valid_token}` }
            };

            res = await axios.get(url_api + path, config)
            .catch(error => {
                if(failureCallback)
                    failureCallback(error);
                else
                    console.log(error);
            });
        }else{
            res = await axios.get(url_api + path)
            .catch(error => {
                
                if(failureCallback)
                    failureCallback(error);
                else
                    console.error(error);
            });
        }

        return res;
    }

    static async post(path, data, token, failureCallback = null) {

        let valid_token = await this.verifyValidToken(token);

        let config = {
            headers: { Authorization: `Bearer ${valid_token}` }
        };

        let res = await axios.post(url_api + path, data, config)
        .catch(error => {
            
            if(failureCallback)
                failureCallback(error);
            else
                console.error(error);
        });

        return res;
    }

    static async put(path, data, token, failureCallback = null) {

        let valid_token = await this.verifyValidToken(token);

        let config = {
            headers: { Authorization: `Bearer ${valid_token}` }
        };

        let res = await axios.put(url_api + path, data, config)
        .catch(error => {
            
            if(failureCallback)
                failureCallback(error);
            else
                console.error(error);
        });

        return res;
    }

    static async getAddressByText(text, failureCallback = null) {

        let res = await axios.get(
            'http://api.positionstack.com/v1/forward?access_key=19a4fc2e537c2fc552848499d6bf8e25&query=' 
            + text
        ).catch(error => {
            if(failureCallback)
                failureCallback(error)
            else
                console.error(error);
        });

        return res;
    }

    static async getAddressByCoordinate(coord, failureCallback = null) {

        let res = await axios.get(
            'http://api.positionstack.com/v1/reverse?access_key=19a4fc2e537c2fc552848499d6bf8e25& query=' 
            + coord.latitude.toString() 
            + ',' + coord.longitude.toString()
        ).catch(error => {
            if(failureCallback) 
                failureCallback(error);
            else
                console.log(error);
        });

        return res;
    }

    static async newUser(path, data, failureCallback = null) {
        let res = await axios.post(url_api + path, data)
        .catch(async error => {
            if(failureCallback) 
                failureCallback(error)
            else
                console.log(error)
        });

        return res;
    }
}