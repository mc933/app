import React, { Component } from 'react';
import Routes from './src/routes';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from './src/commom/Axios';
import { AppLoading } from 'expo';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import * as firebase from 'firebase/app';
import 'firebase/database';

const config = {
	apiKey: "AIzaSyCdQ2s4uVGovuceRpXfbqNdYVeLTyG-RYU",
	authDomain: "mc933-5f3df.firebaseapp.com/",
	databaseURL: "https://mc933-5f3df.firebaseio.com/",
	storageBucket: "mc933-5f3df.appspot.com"
};
firebase.initializeApp(config);

// Get a reference to the database service
const db = firebase.database();

const theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: '#2d75e4',
		accent: 'black',
	}
}

Notifications.setNotificationHandler({
	handleNotification: async () => ({
		shouldShowAlert: true,
		shouldPlaySound: false,
		shouldSetBadge: false,
	}),
});

const types = [
	'Nova requisição para sua oferta!',
	'Nova oferta para sua requisição',
	'Requisição completa',
	'Oferta finalizada'
]

// Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/dashboard/notifications
async function sendPushNotification(type, doc, token, user) {
	if (user !== doc.user) {
		return;
	}

	const message = {
		to: token,
		sound: 'default',
		title: types[type],
		body: types[type],
		data: { data: 'Clique para abrir o aplicativo' },
	};

	await fetch('https://exp.host/--/api/v2/push/send', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Accept-encoding': 'gzip, deflate',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(message),
	});
}

async function registerForPushNotificationsAsync() {
	let token;
	if (Constants.isDevice) {
		const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
		let finalStatus = existingStatus;
		if (existingStatus !== 'granted') {
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
		}
		if (finalStatus !== 'granted') {
			alert('Failed to get push token for push notification!');
			return;
		}
		token = (await Notifications.getExpoPushTokenAsync()).data;
		console.log(token);
	} else {
		alert('Must use physical device for Push Notifications');
	}

	if (Platform.OS === 'android') {
		Notifications.setNotificationChannelAsync('default', {
			name: 'default',
			importance: Notifications.AndroidImportance.MAX,
			vibrationPattern: [0, 250, 250, 250],
			lightColor: '#FF231F7C',
		});
	}

	return token;
}

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			expoPushToken: '',
			loadingComplete: false,
			notification: false
		};

		this.notificationListener = React.createRef();
		this.responseListener = React.createRef();
	}

	async componentDidMount() {

		console.log('APP START\n');
		//await AsyncStorage.setItem('user_token', 'FALSE');
		//await AsyncStorage.setItem('user', 'NONE');
		await AsyncStorage.setItem('screen', 'NONE');
		await AsyncStorage.setItem('sub_screen', 'NONE');
		//await AsyncStorage.setItem('address', 'NONE');
		await AsyncStorage.setItem('GPSAddress', 'NONE');
		await AsyncStorage.setItem('help_offer', 'NONE');
		await AsyncStorage.setItem('help_req', 'NONE');
		await AsyncStorage.setItem('help_req_apply', 'NONE');

		// Verificando TOKEN
		let token = await AsyncStorage.getItem('user_token');

		let user = null;
		if (token == null)
			await AsyncStorage.setItem('user_token', 'FALSE');
		else if (token !== 'FALSE') {
			user = await Axios.get('me', token);
			await AsyncStorage.setItem('user', JSON.stringify(user.data));
		}

		// Verificando ENDEREÇO
		let address = await AsyncStorage.getItem('address');
		if (address == null)
			await AsyncStorage.setItem('address', 'NONE');
		else if (address !== 'NONE')
			address = JSON.parse(address);

		// Verificando ENDEREÇO GPS
		let GPSAddress = await AsyncStorage.getItem('GPSAddress');
		if (GPSAddress == null)
			await AsyncStorage.setItem('GPSAddress', 'NONE')
		else if (GPSAddress !== 'NONE')
			GPSAddress = JSON.parse(GPSAddress);

		this.setState({ loadingComplete: true })

		// pede permissao de notificacoes push
		registerForPushNotificationsAsync().then(token => this.setState({ expoPushToken: token }));

		// This listener is fired whenever a notification is received while the app is foregrounded
		this.notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
			this.setState({ notification });
		});

		// This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
		this.responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
			console.log(response);
		});

		db.ref('notification/new_request_to_offer')
			.on('value',
				(doc) => {
					if (user) sendPushNotification(0, doc.val(), this.state.expoPushToken, user.data)
				});
		db.ref('notification/new_offer_to_request')
			.on('value',
				(doc) => {
					if (user) sendPushNotification(1, doc.val(), this.state.expoPushToken, user.data)
				});
		db.ref('notification/fulfilled')
			.on('value',
				(doc) => {
					if (user) sendPushNotification(2, doc.val(), this.state.expoPushToken, user.data);
				});
		db.ref('notification/finished')
			.on('value',
				(doc) => {
					if (user) sendPushNotification(3, doc.val(), this.state.expoPushToken, user.data)
				});
	}

	render() {

		if (this.state.loadingComplete) {
			return (
				<PaperProvider theme={theme}>
					<Routes />
				</PaperProvider>
			);
		} else {
			return (
				<AppLoading />
			);
		}


	}
}