# Isolamento Solidário

Sabemos que o isolamento social é a melhor prevenção contra o coronavírus nesse momento, então quanto menos pessoas vulneráveis precisarem sair do isolamento, mais protegidas elas estarão.
Pensando nisso, desenvolvemos um aplicativo que visa conectar pessoas que não são de risco, com pessoas vulneráveis, para que as do primeiro grupo possam fazer as compras necessárias para as do segundo grupo, diminuindo o risco destas. 

No aplicativo uma pessoa vulnerável pode escolher uma ajuda já oferecida numa lista, ou solicitar para que seja ajudada em uma opção ainda não disponível.
Da mesma forma, um ajudante pode escolher ajudar uma pessoa que já solicitou uma compra específica, ou publicar uma ajuda disponível diferente das que foram solicitadas e esta vai constar na lista de ajudas já oferecidas. No app também é possível verificar as próximas ajudas que acontecerão e o histórico de ajudas.

## App
* O aplicativo foi feito em [react-native](https://reactnative.dev/) , com a biblioteca [react-native-paper](https://callstack.github.io/react-native-paper/)
* NodeJS 14.9.0
* npm compatível
* Utilizamos o aplicativo [EXPO](https://expo.io/) para rodar a aplicação.

A aplicação estará disponível em localhost:19006

A documentação da API consumida por este app está disponível [aqui](https://gitlab.com/mc933/api).

### Rodar o Aplicativo Localmente
> Instalar o aplicativo EXPO CLI no celular.<br>
> Clonar o repositório instalar dependências com ```npm install``` ou  ```yarn```<br>
> Rodar o app com ```expo start```.<br>
>Se não funcionar tentar ```sudo expo start```.<br>
<br>

>Se der o erro RNCSafeAreaProvider<br>
>> delete node_modules e package-lock.json/ yarn.lock da pasta local<br>
>>alterar a versão do expo em package.json para 38.0.8<br>
>>remover react-native-safe-area-context de package.json<br>
>>instalar dependencias novamente ```npm install``` ou  ```yarn```<br>
>>rodar ```expo install react-native-safe-area-context```<br>


### Rodar a API Localmente

>Instale Python >= 3.6<br>
>Instale o Poetry<br>
>Clone esse projeto e instale suas dependências com ```poetry install```<br>
>Rode a API com ```poetry run uvicorn api.main:app --reload```<br>


### Prototipagem

A prototipagem do projeto com fluxo de telas e features futuras se encontram no software [Miro](https://miro.com/app/board/o9J_krNtEr4=/) com o título V2 fluxo.


### Contribuição

Julia Carnelli - UI/UX + front + ideação e prototipagem

Leonardo Moreira - UI/UX + front + ideação e prototipagem

Felipe Rodrigues - backend + ideação e prototipagem

Daniela Morais - backend + front + ideação e prototipagem

## Link cruzado para os repos (api e app)
https://gitlab.com/mc933/app<br>
https://gitlab.com/mc933/api

# TODO
* Atualização automática do histórico qndo um pedido ou oferecimento de ajuda é finalizado
* Trocar o label de quantidade para input na Todoist
* Notificações para os usuários
* Feedback de erro específico nas chamadas
* Deletar endereço

# Alguns problemas
* Usuário pode cadastrar mesmo endereço quantas vezes quiser.

* Por a API de localização ser ruim, alguns endereços não são encontrados e outros faltam informações como número da casa e/ou cidade (o que ocasiona alguns erros de variáveis vazias).
